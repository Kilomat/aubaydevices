# AubayDevices

[![N|Solid](https://ionicframework.com/img/ionic-logo.png)](https://aubay.com)
AubayDevices is the application for device management.

### Installation
AubayDevices requires [Node.js](https://nodejs.org/) v6+ to run.

Repository

```sh
$ git clone https://Kilomat@bitbucket.org/Kilomat/aubaydevices.git
```

Install the dependencies and devDependencies and start the server.

```sh
$ cd AubayDevices
$ npm install
```

### Running Your App

Run the app in the browser
```sh
$ ionic serve
```

To run your app, all you have to do is enable USB debugging and Developer Mode on your Android device, then run ionic cordova 
```sh
$ ionic cordova run android
```

For production environments...

```sh
$ ionic cordova run android --prod --release
# or
$ ionic cordova build android --prod --release
```

### Infos environments

Cli packages:

    @ionic/cli-utils  : 1.9.2
    ionic (Ionic CLI) : 3.9.2

Global packages:

    Cordova CLI : 7.0.1 

Local packages:

    @ionic/app-scripts : 1.3.7
    Cordova Platforms  : android 6.2.3 browser 4.1.0 ios 4.4.0
    Ionic Framework    : ionic-angular 3.3.0

System:

    Android SDK Tools : 26.0.1
    Node              : v6.10.0
    npm               : 4.2.0 
    OS                : macOS Sierra
    Xcode             : Xcode 8.3.2 Build version 8E2002 