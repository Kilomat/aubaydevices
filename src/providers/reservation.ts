/**
 * Created by hbaha on 31/07/2017.
 */
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from "./user";
import {Api} from "./api";
import {AlertController, Loading, LoadingController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";
import {HistoryService} from "./history";
import {Devices} from "./devices";

@Injectable()
export class ReservationService {
    loading: Loading;

    constructor(public http: Http, public user: User, public api: Api, private alertCtrl: AlertController,
                private loadingCtrl: LoadingController, public translateS: TranslateService, public historyService: HistoryService,
                public devicesService: Devices) {
    }

    /**
     * api {post} /devices/search Find available devices
     * Permission user
     *
     * Param {String} type Type of device(s).
     * Param {String} os Os of device(s).
     * Param {String} from Time pick-up.
     * Param {String} to Time return.
     *
     * @apiSuccess {String} message Response message.
     *
     */

    getAvailableDevices(data) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return new Promise(resolve => {
            let seq = this.api.post('devices/search', data, options).share();
            seq.map(res => res.json()).subscribe(data => {
                let newdata: any = this.user.AddUrlToPicture(data);
                resolve(newdata);
            });
        });
    }

    checkAvailabilityDevice(data) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return new Promise(resolve => {
            let seq = this.api.post('devices/search/' + data._id, data, options).share();
            seq.map(res => res.json()).subscribe(data => {
                let newdata: any = this.user.AddUrlToPicture(data);
                resolve(newdata);
            });
        });
    }

    /**
     * api {post} /devices/reseve Booking a device
     * Permission user
     * Name BookingDevice
     * Group Device
     *
     * Description Before booking a device, please check the availability of the device.
     *
     *
     * Param {String} _id Id of device.
     * Param {String} userId Id of User.
     * Param {String} from Time pick-up.
     * Param {String} to Time return.
     *
     * @apiSuccess {String} message Response message.
     *
     */

    reserveDevice(data) {
        let headersx = new Headers();
        headersx.append('x-access-token', this.user._user.token);
        let options = new RequestOptions({headers: headersx});

        return new Promise(resolve => {
            let seq = this.api.post('devices/reserve', data, options).share();
            seq.map(res => res.json()).subscribe(data => {
                resolve(data);
            });
        });
    }

    availabilityChecking(id, from, to, item) {
        let loading = this.loadingCtrl.create({
            content: "Availability checking..."
        });

        loading.present();

        let options = {
            _id: id,
            from: from,
            to: to,
            userId: this.user._user.idUser
        };

        this.checkAvailabilityDevice(options).then((data) => {
            loading.dismiss();

            if (typeof(data[0]) === "undefined") {
                let alert = this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: this.translateS.get('ALERT_SEARCH_DEVICE_ID')['value'],
                    buttons: ['Ok']
                });
                alert.present();
            } else {
                if (item) {
                    item.to = options.to;
                    this.extendTheReservation(item);
                }
                else {
                    this.bookingTheDevice(options);
                }
            }
        }, (err) => {
        });
    }

    extendTheReservation(item) {
        this.devicesService.extendReservation(item);
        this.historyService.extendHistory(item);
    }

    bookingTheDevice(options) {
        let loading = this.loadingCtrl.create({
            content: "Booking device..."
        });

        loading.present();

        this.reserveDevice(options).then((res) => {
            this.addReservationToHistory(res);
            loading.dismiss();
            //this.nav.popToRoot();
        }, (err) => {
        });
    }

    addReservationToHistory(infos) {
        let data = {};
        data["idDevice"] = infos.idDevice;
        data["idUser"] = this.user._user.idUser;
        data["idReservation"] = infos.idReservation;
        data["timereturn"] = infos.to;
        data["timepickup"] = infos.from;
        data["token"] = this.user._user.token;
        let headers = new Headers({'x-access-token': this.user._user.token});
        this.historyService.createHistory(data, headers);
    }

}
