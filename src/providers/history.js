"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
var HistoryService = (function () {
    function HistoryService(http, api, user, toastRes) {
        this.http = http;
        this.api = api;
        this.user = user;
        this.toastRes = toastRes;
    }
    /**
     * api {get} /histories/ Get all histories
     * Name GetHistories
     * Group History
     *
     * @apiSuccess {String} idReservation id of reservation.
     * @apiSuccess {String} idUser id of user.
     * @apiSuccess {String} idDevice id of device.
     * @apiSuccess {String} timereturn Time return.
     * @apiSuccess {String} timepickup Time pickup.
     *
     */
    HistoryService.prototype.getHistories = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.api.get('histories')
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.data = data;
                resolve(_this.data);
            });
        });
    };
    /**
     * api {delete} /users/histories/:idHistory Delete a reservation from an user
     * Permission user
     * Name DeleteReservationFromUser
     * Group History
     *
     * Param {Number} idHistory History that you want to delete.
     * Param {String} token authentification token is mandatory.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     *
     */
    HistoryService.prototype.returnDevice = function (idHistory) {
        var _this = this;
        var headersx = new http_1.Headers();
        headersx.append('x-access-token', this.user._user.token);
        var options = new http_1.RequestOptions({ headers: headersx });
        var seq = this.api.delete('users/histories/' + idHistory, options).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.success) {
                _this.toastRes.presentToast('Success : The device has been successfully returned !');
            }
            else {
                _this.toastRes.presentToast('Warning : ' + res.message);
            }
        }, function (err) {
            err = JSON.parse(err._body);
            _this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
        });
        return seq;
    };
    /**
     * api {post} /histories Add an history
     * Name AddHistory
     * Group History
     *
     * Param {String} timepickup Time pickup.
     * Param {String} timereturn Time return.
     * Param {String} idDevice Id of the device.
     * Param {String} idUser Id of the user.
     * Param {String} idReservation Id of the reservation.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     */
    HistoryService.prototype.createHistory = function (Infos, options) {
        var _this = this;
        var seq = this.api.post('histories', Infos, options).share();
        seq.map(function (res) { return res.json(); }).subscribe(function (res) {
            if (res.success)
                _this.toastRes.presentToast('Success : The device has been successfully added to your reservation');
        }, function (err) {
            err = JSON.parse(err._body);
            _this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
        });
    };
    return HistoryService;
}());
HistoryService = __decorate([
    core_1.Injectable()
], HistoryService);
exports.HistoryService = HistoryService;
