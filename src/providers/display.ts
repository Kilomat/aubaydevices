/**
 * Created by hbaha on 28/06/2017.
 */

import {Loading, LoadingController, ToastController} from "ionic-angular";
import {Injectable} from '@angular/core';


@Injectable()
export class Display {

  loading: Loading;
  constructor(private toastCtrl: ToastController, private loadingCtrl: LoadingController) {
  }

  /**
   * Display a notification with params(message)
   */
  public presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 6000,
      position: 'top',
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }

  /**
  * Alert Loading.
  */
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
}
