"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by hbaha on 31/07/2017.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var ReservationService = (function () {
    function ReservationService(http, user, api) {
        this.http = http;
        this.user = user;
        this.api = api;
    }
    /**
     * api {post} /devices/search Find available devices
     * Permission user
     *
     * Param {String} type Type of device(s).
     * Param {String} os Os of device(s).
     * Param {String} from Time pick-up.
     * Param {String} to Time return.
     *
     * @apiSuccess {String} message Response message.
     *
     */
    ReservationService.prototype.getAvailableDevices = function (data) {
        var _this = this;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return new Promise(function (resolve) {
            var seq = _this.api.post('devices/search', data, options).share();
            seq.map(function (res) { return res.json(); }).subscribe(function (data) {
                var newdata = _this.AddUrlToPicture(data);
                resolve(newdata);
            });
        });
    };
    /**
     * api {post} /devices/reseve Booking a device
     * Permission user
     * Name BookingDevice
     * Group Device
     *
     * Description Before booking a device, please check the availability of the device.
     *
     *
     * Param {String} _id Id of device.
     * Param {String} userId Id of User.
     * Param {String} from Time pick-up.
     * Param {String} to Time return.
     *
     * @apiSuccess {String} message Response message.
     *
     */
    ReservationService.prototype.reserveDevice = function (data) {
        var _this = this;
        var headersx = new http_1.Headers();
        headersx.append('x-access-token', this.user._user.token);
        var options = new http_1.RequestOptions({ headers: headersx });
        return new Promise(function (resolve) {
            var seq = _this.api.post('devices/reserve', data, options).share();
            seq.map(function (res) { return res.json(); }).subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReservationService.prototype.AddUrlToPicture = function (data) {
        var newData = [];
        for (var i in data) {
            if (data[i]["picture"]) {
                data[i]["picture"] = this.api.url + data[i]["picture"];
            }
        }
        return newData = data;
    };
    return ReservationService;
}());
ReservationService = __decorate([
    core_1.Injectable()
], ReservationService);
exports.ReservationService = ReservationService;
