import {Injectable, Input} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {Api} from './api';
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Display} from "./display";

@Injectable()
export class User {

  dataAll: any;
  dataUser: any;
  _user: { idUser: string, email: string, token: string, admin: boolean } = {
    idUser: '', email: '', token: '', admin: false};

  constructor(public http: Http, public api: Api, public toastRes: Display) {
  }

  /**
   * api {post} /users/authenticate Authenticate an user
   * Name AuthenticateUser
   * Group User
   *
   * Param {String} email Email to be authentified.
   * Param {String} password Password to be authentified.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   * @apiSuccess {String} token Token of authentification.
   * @apiSuccess {Number} id Id of user.
   *
   */

  login(accountInfo: any) {
    let seq = this.api.post('users/authenticate', accountInfo).share();

    seq.map(res => res.json()).subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
        if (res.success) {
          this._loggedIn(res, accountInfo);
        } else {
        }
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  /**
   * api {post} /users Add an user
   * Name AddUser
   * Group User
   *
   * Param {String} email Email of the user.
   * Param {String} password Password of the user.
   * Param {String} name Name of the user.
   * Param {String} phone Phone of the user.
   * Param {String} [status] Status of the user.
   * Param {String} [admin] Admin.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */

  newUser(Infos: any) {
    let seq = this.api.post('users', Infos).share();

    seq.map(res => res.json()).subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
        if (res.success) {
          this.toastRes.presentToast('Success : The user has been successfully created');
        } else {
          this.toastRes.presentToast('Warning : ' + res.message);
        }
      }, err => {
        err = JSON.parse(err._body);
       this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
      });

    return seq;
  }


  /**
   * api {delete} /users/:idUser Delete an user by id
   * Permission user
   * Name DeleteUserById
   * Group User
   *
   * Param {Number} idUser User that you want to delete.
   * Param {String} token authentification token is mandatory.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */

  deleteUser(iduser: any) {
    let headersx = new Headers();
    headersx.append('x-access-token', this._user.token);

    let options = new RequestOptions({headers: headersx});
    let seq = this.api.delete('users/' + iduser, options).share();

    seq.map(res => res.json()).subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
        if (res.success) {
          this.toastRes.presentToast('Success : The user has been successfully deleted');
        } else {
          this.toastRes.presentToast('Warning : ' + res.message);
        }
      }, err => {
        err = JSON.parse(err._body);
        this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
      });

    return seq;
  }

  /**
   * api {put} /users/:idUser Edit an user
   * Permission user
   * Name EditUser
   * Group User
   *
   * Param {Number} idUser User you want to edit.
   * Param {String} email Email of the user.
   * Param {String} [password] Password of the user.
   * Param {String} [name] Name of the user.
   * Param {String} [phone] Phone of the user.
   * Param {String} [status] Status of the user.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */

  changeUser(Infos: any) {

    let userId = Infos._id;
    let headersx = new Headers();

    headersx.append('x-access-token', this._user.token);
    let options = new RequestOptions({headers: headersx});
    let seq = this.api.put('users/' + userId, Infos, options).share();

    seq.map(res => res.json()).subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
        if (res.success) {
          this.toastRes.presentToast('Success: The profile has been successfully changed.');
        } else {
          this.toastRes.presentToast('Warning : ' + res.message);
        }
      }, err => {
        err = JSON.parse(err._body);
        this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
      });
    return seq;
  }

  /**
   * api {get} /users/ Get all users
   * Name GetUsers
   * Group User
   *
   * @apiSuccess {String} name Name of the user.
   * @apiSuccess {String} email Email of the user.
   * @apiSuccess {String} phone Phone of the user.
   * @apiSuccess {String} status Status of the user.
   * @apiSuccess {String} picture Picture URL of the user.
   */

  getAllUsers() {
    return new Promise(resolve => {
      this.api.get('users')
        .map(res => res.json())
        .subscribe(data => {
          let newdata : any = this.AddUrlToPicture(data);
          this.setDataUsers(newdata);
          resolve(newdata);
        });
    });
  }

  /**
   * api {get} /users/:idUser Get an user by id
   * Name GetUsersById
   * Group User
   *
   * Param {Number} idUser User you want to get.
   *
   * @apiSuccess {String} name Name of the user.
   * @apiSuccess {String} email Email of the user.
   * @apiSuccess {String} phone Phone of the user.
   * @apiSuccess {String} status Status of the user.
   * @apiSuccess {String} picture Picture URL of the user.
   */

  getuser(userId) {
    // don't have the data yet
    return new Promise(resolve => {
      this.api.get('users/' + userId)
        .map(res => res.json())
        .subscribe(data => {
          let newdata : any = this.AddUrlToPicture(data);
          this.dataUser = newdata;
          resolve(newdata);
        });
    });
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }


  /**
   * Process a login-service/login response to store user data
   */
  _loggedIn(resp, infos) {
    this._user.idUser = resp.id;
    this._user.token = resp.token;
    this._user.email = infos.email;
    this._user.admin = resp.admin;
  }

  /**
   * Process a users response to search user
   */
  query(params?: any) {
    if (!params) {
      return this.dataAll;
    }

    return this.dataAll.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  setDataUsers(data){
    this.dataAll = data;
  }

  AddUrlToPicture(data){
    let newData = [];

    if (data.length > 1){
      for (let i in data)
        if (data[i]["picture"]){
          if (!/^https?:\/\//i.test(data[i]["picture"])) {
            data[i]["picture"] = this.api.url + data[i]["picture"];
          }
        }
    }
    else{
      if (!/^https?:\/\//i.test(data["picture"])) {
        data["picture"] = this.api.url + data["picture"];
      }
    }
    return newData = data;
  }
}
