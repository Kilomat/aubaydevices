/**
 * Created by hbaha on 09/10/2017.
 */


import {Injectable} from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController } from "ionic-angular";


@Injectable()
export class NetworkService {

    public disconnect: boolean = false;

    constructor(private network: Network, public alertctrl: AlertController) {
        // watch network for a disconnect
        network.onDisconnect().subscribe(() => {
            this.disconnect = true;
            this.checkNetwork();
            console.log('network was disconnected :-(');
        });

        network.onConnect().subscribe(() => {
            this.disconnect = false;
        });

        // stop disconnect watch
        //disconnectSubscription.unsubscribe();
    }

    checkNetwork() {
        let alert = this.alertctrl.create({
            title: 'Connexion...',
            subTitle: 'Vous êtes actuellement hors connexion!',
            buttons: ['OK']
        });
        alert.present();
    }
}