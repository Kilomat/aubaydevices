import { Injectable } from '@angular/core';
import {Http, Headers,  RequestOptions} from '@angular/http';
import { Api } from './api';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {User} from "./user";
import {Display} from "./display";

@Injectable()
export class Devices {
  data : any;

  constructor(public http: Http, public api: Api, public user:User, public toastRes: Display) {
  }

  /**
   * api {get} /devices/ Get all devices
   * Name GetDevices
   * Group Device
   *
   * @apiSuccess {String} slug Slug of the device.
   * @apiSuccess {String} status Status of the device.
   * @apiSuccess {String} version Version of the device.
   * @apiSuccess {Number} density Price of the device.
   * @apiSuccess {String} screen Screen size of the device.
   * @apiSuccess {Number} width Width of the device.
   * @apiSuccess {Number} height Height of the device.
   * @apiSuccess {String} type Type of the device.
   * @apiSuccess {String} os Os of the device.
   * @apiSuccess {String} IdFactory Factory id of the device.
   * @apiSuccess {String} brand Brand of the device.
   * @apiSuccess {String} name Name of the device.
   * @apiSuccess {Object} reserved List of reservations.
   * @apiSuccess {String} picture Picture of the device (path to image file).
   *
   **/

  getAllDevices() {
    return new Promise(resolve => {
      this.api.get('devices')
        .map(res => res.json())
        .subscribe(data => {
          let newdata : any = this.user.AddUrlToPicture(data);
          this.setDataDevices(newdata);
          resolve(newdata);
        });
    });
  }

  /**
   * api {post} /devices Add an device
   * Permission admin/user
   * Name AddDevice
   * Group Device
   *
   * Param {String} IdFactory Factory id of the device..
   * Param {String} brand Brand of the device.
   * Param {String} name Name of the device.
   * Param {String} os Os of the device.
   * Param {String} version Version of the device.
   * Param {String} screen Screen size of the device.
   * Param {Number} width Width of the device.
   * Param {Number} height Height of the device.
   * Param {String} type Type of the device.
   * Param {Number} density Price of the device.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */

  newDevice(Infos: any) {
    let headersx = new Headers();
    headersx.append('x-access-token', this.user._user.token);
    let options = new RequestOptions({ headers: headersx});

    let seq = this.api.post('devices', Infos, options).share();
    seq.map(res => res.json()).subscribe(res => {
        if (res.success) {
          this.toastRes.presentToast('Success : The device has been successfully created');
        } else {
          this.toastRes.presentToast('Warning : ' + res.message);
        }
      }, err => {
        err = JSON.parse(err._body);
        this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
      });

    return seq;
  }

  /**
   * api {put} /devices/:idReservation Edit an reservation
   * Permission user
   * Name Extend the reservation
   * Group Device
   *
   * Param {Number} idDevice Device you want to edit.
   *
   * @apiSuccess {String} [idReservation] id of reservation.
   * @apiSuccess {String} [timereturn] Time return.
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */

   extendReservation(Infos: any) {

    let headersx = new Headers();

    headersx.append('x-access-token', this.user._user.token);
    let options = new RequestOptions({headers: headersx});
    let seq = this.api.put('devices/extend/' + Infos._id, Infos, options).share();

    seq.map(res => res.json()).subscribe(res => {
      // If the API returned a successful response, mark the user as logged in
      if (res.success) {
        this.toastRes.presentToast('Success: The reservation has been successfully changed.');
      } else {
        this.toastRes.presentToast('Warning : ' + res.message);
      }
    }, err => {
      err = JSON.parse(err._body);
      this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
    });
    return seq;
  }

  /**
   * Process a devices response to search device
   */
  query(params?: any) {
    if (!params)
      return this.data;

    return this.data.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  setDataDevices(data){
    this.data = data;
  }

}
