"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
var Devices = (function () {
    function Devices(http, api, user, toastRes) {
        this.http = http;
        this.api = api;
        this.user = user;
        this.toastRes = toastRes;
    }
    /**
     * api {get} /devices/ Get all devices
     * Name GetDevices
     * Group Device
     *
     * @apiSuccess {String} slug Slug of the device.
     * @apiSuccess {String} status Status of the device.
     * @apiSuccess {String} version Version of the device.
     * @apiSuccess {Number} density Price of the device.
     * @apiSuccess {String} screen Screen size of the device.
     * @apiSuccess {Number} width Width of the device.
     * @apiSuccess {Number} height Height of the device.
     * @apiSuccess {String} type Type of the device.
     * @apiSuccess {String} os Os of the device.
     * @apiSuccess {String} IdFactory Factory id of the device.
     * @apiSuccess {String} brand Brand of the device.
     * @apiSuccess {String} name Name of the device.
     * @apiSuccess {Object} reserved List of reservations.
     * @apiSuccess {String} picture Picture of the device (path to image file).
     *
     **/
    Devices.prototype.getAllDevices = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.api.get('devices')
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                var newdata = _this.AddUrlToPicture(data);
                _this.setDataDevices(newdata);
                resolve(newdata);
            });
        });
    };
    /**
     * api {post} /devices Add an device
     * Permission admin/user
     * Name AddDevice
     * Group Device
     *
     * @apiParam {String} IdFactory Factory id of the device..
     * @apiParam {String} brand Brand of the device.
     * @apiParam {String} name Name of the device.
     * @apiParam {String} os Os of the device.
     * @apiParam {String} version Version of the device.
     * @apiParam {String} screen Screen size of the device.
     * @apiParam {Number} width Width of the device.
     * @apiParam {Number} height Height of the device.
     * @apiParam {String} type Type of the device.
     * @apiParam {Number} density Price of the device.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     */
    Devices.prototype.newDevice = function (Infos) {
        var _this = this;
        var headersx = new http_1.Headers();
        headersx.append('x-access-token', this.user._user.token);
        var options = new http_1.RequestOptions({ headers: headersx });
        var seq = this.api.post('devices', Infos, options).share();
        seq.map(function (res) { return res.json(); }).subscribe(function (res) {
            if (res.success) {
                _this.toastRes.presentToast('Success : The device has been successfully created');
            }
            else {
                _this.toastRes.presentToast('Warning : ' + res.message);
            }
        }, function (err) {
            err = JSON.parse(err._body);
            _this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
        });
        return seq;
    };
    /**
     * Process a devices response to search device
     */
    Devices.prototype.query = function (params) {
        if (!params)
            return this.data;
        return this.data.filter(function (item) {
            for (var key in params) {
                var field = item[key];
                if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
                    return item;
                }
                else if (field == params[key]) {
                    return item;
                }
            }
            return null;
        });
    };
    Devices.prototype.setDataDevices = function (data) {
        this.data = data;
    };
    Devices.prototype.AddUrlToPicture = function (data) {
        var newData = [];
        for (var i in data) {
            if (data[i]["picture"]) {
                data[i]["picture"] = this.api.url + data[i]["picture"];
            }
        }
        return newData = data;
    };
    return Devices;
}());
Devices = __decorate([
    core_1.Injectable()
], Devices);
exports.Devices = Devices;
