"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
var User = (function () {
    function User(http, api, toastRes) {
        this.http = http;
        this.api = api;
        this.toastRes = toastRes;
        this._user = {
            idUser: '', email: '', token: '', admin: false
        };
    }
    /**
     * api {post} /users/authenticate Authenticate an user
     * Name AuthenticateUser
     * Group User
     *
     * Param {String} email Email to be authentified.
     * Param {String} password Password to be authentified.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     * @apiSuccess {String} token Token of authentification.
     * @apiSuccess {Number} id Id of user.
     *
     */
    User.prototype.login = function (accountInfo) {
        var _this = this;
        var seq = this.api.post('users/authenticate', accountInfo).share();
        seq.map(function (res) { return res.json(); }).subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.success) {
                _this._loggedIn(res, accountInfo);
            }
            else {
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    /**
     * @api {post} /users Add an user
     * Name AddUser
     * Group User
     *
     * Param {String} email Email of the user.
     * Param {String} password Password of the user.
     * Param {String} name Name of the user.
     * Param {String} phone Phone of the user.
     * Param {String} [status] Status of the user.
     * Param {String} [admin] Admin.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     */
    User.prototype.newUser = function (Infos) {
        var _this = this;
        var seq = this.api.post('users', Infos).share();
        seq.map(function (res) { return res.json(); }).subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.success) {
                _this.toastRes.presentToast('Success : The user has been successfully created');
            }
            else {
                _this.toastRes.presentToast('Warning : ' + res.message);
            }
        }, function (err) {
            err = JSON.parse(err._body);
            _this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
        });
        return seq;
    };
    /**
     * api {delete} /users/:idUser Delete an user by id
     * Permission user
     * Name DeleteUserById
     * Group User
     *
     * Param {Number} idUser User that you want to delete.
     * Param {String} token authentification token is mandatory.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     */
    User.prototype.deleteUser = function (iduser) {
        var _this = this;
        var headersx = new http_2.Headers();
        headersx.append('x-access-token', this._user.token);
        var options = new http_1.RequestOptions({ headers: headersx });
        var seq = this.api.delete('users/' + iduser, options).share();
        seq.map(function (res) { return res.json(); }).subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.success) {
                _this.toastRes.presentToast('Success : The user has been successfully deleted');
            }
            else {
                _this.toastRes.presentToast('Warning : ' + res.message);
            }
        }, function (err) {
            err = JSON.parse(err._body);
            _this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
        });
        return seq;
    };
    /**
     * api {put} /users/:idUser Edit an user
     * Permission user
     * Name EditUser
     * Group User
     *
     * Param {Number} idUser User you want to edit.
     * Param {String} email Email of the user.
     * Param {String} [password] Password of the user.
     * Param {String} [name] Name of the user.
     * Param {String} [phone] Phone of the user.
     * Param {String} [status] Status of the user.
     *
     * @apiSuccess {Boolean} success Notify the success of current request.
     * @apiSuccess {String} message Response message.
     */
    User.prototype.changeUser = function (Infos) {
        var _this = this;
        var userId = Infos._id;
        var headersx = new http_2.Headers();
        headersx.append('x-access-token', this._user.token);
        var options = new http_1.RequestOptions({ headers: headersx });
        var seq = this.api.put('users/' + userId, Infos, options).share();
        seq.map(function (res) { return res.json(); }).subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.success) {
                _this.toastRes.presentToast('Success: The profile has been successfully changed.');
            }
            else {
                _this.toastRes.presentToast('Warning : ' + res.message);
            }
        }, function (err) {
            err = JSON.parse(err._body);
            _this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
        });
        return seq;
    };
    /**
     * api {get} /users/ Get all users
     * Name GetUsers
     * Group User
     *
     * @apiSuccess {String} name Name of the user.
     * @apiSuccess {String} email Email of the user.
     * @apiSuccess {String} phone Phone of the user.
     * @apiSuccess {String} status Status of the user.
     * @apiSuccess {String} picture Picture URL of the user.
     */
    User.prototype.getAllUsers = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.api.get('users')
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                var newdata = _this.AddUrlToPicture(data);
                _this.setDataUsers(newdata);
                resolve(newdata);
            });
        });
    };
    /**
     * api {get} /users/:idUser Get an user by id
     * Name GetUsersById
     * Group User
     *
     * Param {Number} idUser User you want to get.
     *
     * @apiSuccess {String} name Name of the user.
     * @apiSuccess {String} email Email of the user.
     * @apiSuccess {String} phone Phone of the user.
     * @apiSuccess {String} status Status of the user.
     * @apiSuccess {String} picture Picture URL of the user.
     */
    User.prototype.getuser = function (userId) {
        var _this = this;
        // don't have the data yet
        return new Promise(function (resolve) {
            _this.api.get('users/' + userId)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                var newdata = _this.AddUrlToPicture(data);
                _this.dataUser = newdata;
                resolve(newdata);
            });
        });
    };
    /**
     * Log the user out, which forgets the session
     */
    User.prototype.logout = function () {
        this._user = null;
    };
    /**
     * Process a login-service/signup response to store user data
     */
    User.prototype._loggedIn = function (resp, infos) {
        this._user.idUser = resp.id;
        this._user.token = resp.token;
        this._user.email = infos.email;
        this._user.admin = resp.admin;
    };
    /**
     * Process a users response to search user
     */
    User.prototype.query = function (params) {
        if (!params) {
            return this.dataAll;
        }
        return this.dataAll.filter(function (item) {
            for (var key in params) {
                var field = item[key];
                if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
                    return item;
                }
                else if (field == params[key]) {
                    return item;
                }
            }
            return null;
        });
    };
    User.prototype.setDataUsers = function (data) {
        this.dataAll = data;
    };
    User.prototype.AddUrlToPicture = function (data) {
        var newData = [];
        if (data.length > 1) {
            for (var i in data)
                if (data[i]["picture"]) {
                    data[i]["picture"] = this.api.url + data[i]["picture"];
                }
        }
        else
            data["picture"] = this.api.url + data["picture"];
        return newData = data;
    };
    return User;
}());
User = __decorate([
    core_1.Injectable()
], User);
exports.User = User;
