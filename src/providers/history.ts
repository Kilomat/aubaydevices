import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Api} from './api';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {User} from "./user";
import {Display} from "./display";


@Injectable()
export class HistoryService {
  data: any;

  constructor(public http: Http, public api: Api, public user: User, public toastRes: Display) {
  }

  /**
   * api {get} /histories/ Get all histories
   * Name GetHistories
   * Group History
   *
   * @apiSuccess {String} idReservation id of reservation.
   * @apiSuccess {String} idUser id of user.
   * @apiSuccess {String} idDevice id of device.
   * @apiSuccess {String} timereturn Time return.
   * @apiSuccess {String} timepickup Time pickup.
   *
   */
  getHistories() {
    return new Promise(resolve => {
      this.api.get('histories')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  /**
   * api {delete} /users/histories/:idHistory Delete a reservation from an user
   * Permission user
   * Name DeleteReservationFromUser
   * Group History
   *
   * Param {Number} idHistory History that you want to delete.
   * Param {String} token authentification token is mandatory.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   *
   */
  returnDevice(idHistory: any) {
    let headersx = new Headers();
    headersx.append('x-access-token', this.user._user.token);
    let options = new RequestOptions({ headers: headersx});

    let seq = this.api.delete('users/histories/' + idHistory, options).share();
    seq
      .map(res => res.json())
      .subscribe(res => {
        if (res.success) {
          this.toastRes.presentToast('Success : The device has been successfully returned !');
        } else {
          this.toastRes.presentToast('Warning : ' + res.message);
        }
      }, err => {
        err = JSON.parse(err._body);
        this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
      });

    return seq;
  }

  /**
   * api {post} /histories Add an history
   * Name AddHistory
   * Group History
   *
   * Param {String} timepickup Time pickup.
   * Param {String} timereturn Time return.
   * Param {String} idDevice Id of the device.
   * Param {String} idUser Id of the user.
   * Param {String} idReservation Id of the reservation.
   *
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */
  createHistory(Infos: any, options: any) {
    let seq = this.api.post('histories', Infos, options).share();
    seq.map(res => res.json()).subscribe(res => {
      if (res.success)
        this.toastRes.presentToast('Success : The device has been successfully added to your reservation');
    }, err => {
      err = JSON.parse(err._body);
      this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
    });
  }

  /**
   * api {put} /histories/extend/:idReservation Extend an reservation
   * Permission user
   * Name ExtendReservation
   * Group History
   *
   * Param {Number} idReservation Reservation you want to edit.
   *
   * @apiSuccess {String} [timereturn] Time return.
   * @apiSuccess {Boolean} success Notify the success of current request.
   * @apiSuccess {String} message Response message.
   */

  extendHistory(Infos: any) {

    let headersx = new Headers();
    headersx.append('x-access-token', this.user._user.token);
    let options = new RequestOptions({headers: headersx});
    let seq = this.api.put('histories/extend/' + Infos.idreservation, Infos, options).share();

    seq.map(res => res.json()).subscribe(res => {
      // If the API returned a successful response
    }, err => {
      err = JSON.parse(err._body);
      this.toastRes.presentToast('Une erreur est survenue. ' + err.message);
    });
    return seq;
  }

}
