"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var ionic_angular_1 = require("ionic-angular");
var storage_1 = require("@ionic/storage");
var transfer_1 = require("@ionic-native/transfer");
var app_component_1 = require("./app.component");
var device_detail_1 = require("../pages/devices/devices-detail/device-detail");
var devices_list_1 = require("../pages/devices/devices-list/devices-list");
var login_1 = require("../pages/login/login");
var devices_search_1 = require("../pages/devices/devices-search/devices-search");
var tabs_1 = require("../pages/tabs/tabs");
var api_1 = require("../providers/api");
var user_1 = require("../providers/user");
var camera_1 = require("@ionic-native/camera");
var google_maps_1 = require("@ionic-native/google-maps");
var splash_screen_1 = require("@ionic-native/splash-screen");
var status_bar_1 = require("@ionic-native/status-bar");
var core_2 = require("@ngx-translate/core");
var http_loader_1 = require("@ngx-translate/http-loader");
var reservation_1 = require("../pages/reservation/reservation");
var dashboardAdmin_1 = require("../pages/dashboard/dashboard-admin/dashboardAdmin");
var list_users_1 = require("../pages/users/users-list/list-users");
var users_search_1 = require("../pages/users/users-search/users-search");
var users_create_1 = require("../pages/users/users-create/users-create");
var devices_create_1 = require("../pages/devices/devices-create/devices-create");
var history_1 = require("../pages/history/history");
var user_detail_1 = require("../pages/users/users-detail/user-detail");
var devices_1 = require("../providers/devices");
var history_2 = require("../providers/history");
var dashboardUser_1 = require("../pages/dashboard/dashboard-user/dashboardUser");
var dashboard_1 = require("../pages/dashboard/dashboard");
var profile_1 = require("../pages/dashboard/profile/profile");
var toastRes_1 = require("../providers/toastRes");
var reservation_available_devices_1 = require("../pages/reservation/reservation-available-devices/reservation-available-devices");
var reservation_confirmation_1 = require("../pages/reservation/reservation-confirmation/reservation-confirmation");
var reservation_2 = require("../providers/reservation");
var reservation_search_1 = require("../pages/reservation/reservation-search/reservation-search");
var history_search_1 = require("../pages/history/history-search/history-search");
var history_detail_1 = require("../pages/history/history-detail/history-detail");
var ionic2_calendar_1 = require("ionic2-calendar");
// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
function HttpLoaderFactory(http) {
    return new http_loader_1.TranslateHttpLoader(http, './assets/i18n/', '.json');
}
exports.HttpLoaderFactory = HttpLoaderFactory;
/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
var pages = [
    app_component_1.MyApp,
    devices_list_1.ListMasterPage,
    login_1.LoginPage,
    profile_1.ProfilePage,
    dashboardAdmin_1.DashboardAdminPage,
    dashboardUser_1.DashboardUserPage,
    dashboard_1.DashboardPage,
    devices_search_1.SearchPage,
    tabs_1.TabsPage,
    users_create_1.UsersCreatePage,
    users_search_1.UsersSearchPage,
    list_users_1.ListUsersPage,
    user_detail_1.UserDetailPage,
    device_detail_1.DeviceDetailPage,
    devices_create_1.DevicesCreatePage,
    reservation_1.ReservePage,
    reservation_confirmation_1.BookingPage,
    reservation_available_devices_1.AvailableDevicesPage,
    reservation_search_1.SearchReservePage,
    history_1.HistoryPage,
    history_detail_1.HistoryDetailPage,
    history_search_1.HistorySearchPage
];
function declarations() {
    return pages;
}
exports.declarations = declarations;
function entryComponents() {
    return pages;
}
exports.entryComponents = entryComponents;
function providers() {
    return [
        api_1.Api,
        history_2.HistoryService,
        user_1.User,
        devices_1.Devices,
        reservation_2.ReservationService,
        toastRes_1.ToastRes,
        camera_1.Camera,
        google_maps_1.GoogleMaps,
        splash_screen_1.SplashScreen,
        status_bar_1.StatusBar,
        transfer_1.Transfer,
        // Localisation
        { provide: core_1.LOCALE_ID, useValue: 'fr-fr' },
        // Keep this to enable Ionic's runtime error handling during development
        { provide: core_1.ErrorHandler, useClass: ionic_angular_1.IonicErrorHandler }
    ];
}
exports.providers = providers;
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: declarations(),
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            ionic2_calendar_1.NgCalendarModule,
            core_2.TranslateModule.forRoot({
                loader: {
                    provide: core_2.TranslateLoader,
                    useFactory: HttpLoaderFactory,
                    deps: [http_1.Http]
                }
            }),
            ionic_angular_1.IonicModule.forRoot(app_component_1.MyApp),
            storage_1.IonicStorageModule.forRoot()
        ],
        bootstrap: [ionic_angular_1.IonicApp],
        entryComponents: entryComponents(),
        providers: providers()
    })
], AppModule);
exports.AppModule = AppModule;
