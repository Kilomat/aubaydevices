import { NgModule, ErrorHandler, LOCALE_ID} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { Transfer } from '@ionic-native/transfer';

import { MyApp } from './app.component';

import { DeviceDetailPage } from '../pages/devices/devices-detail/device-detail';
import { ListMasterPage } from '../pages/devices/devices-list/devices-list';
import { LoginPage } from '../pages/login/login';
import { SearchPage } from '../pages/devices/devices-search/devices-search';
import { TabsPage } from '../pages/tabs/tabs';

import { Api } from '../providers/api';
import { User } from '../providers/user';

import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {ReservePage} from "../pages/reservation/reservation";
import {DashboardAdminPage} from "../pages/dashboard/dashboard-admin/dashboardAdmin";
import {ListUsersPage} from "../pages/users/users-list/list-users";
import {UsersSearchPage} from "../pages/users/users-search/users-search";
import {UsersCreatePage} from "../pages/users/users-create/users-create";
import {DevicesCreatePage} from "../pages/devices/devices-create/devices-create";
import {HistoryPage} from "../pages/history/history";
import {UserDetailPage} from "../pages/users/users-detail/user-detail";
import {Devices} from "../providers/devices";
import {HistoryService} from "../providers/history";
import {DashboardUserPage} from "../pages/dashboard/dashboard-user/dashboardUser";
import {DashboardPage} from "../pages/dashboard/dashboard";
import {ProfilePage} from "../pages/dashboard/profile/profile";
import {Display} from "../providers/display";
import {reservationAvailableDevicesPage} from "../pages/reservation/reservation-available-devices/reservation-available-devices";
import {BookingPage} from "../pages/reservation/reservation-confirmation/reservation-confirmation";
import {ReservationService} from "../providers/reservation";
import {SearchReservePage} from "../pages/reservation/reservation-search/reservation-search";
import {HistorySearchPage} from "../pages/history/history-search/history-search";
import {HistoryDetailPage} from "../pages/history/history-detail/history-detail";
import {NgCalendarModule} from "ionic2-calendar";
import {NetworkService} from "../providers/network";
import {Network} from "@ionic-native/network";


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [

  MyApp,
  ListMasterPage,
  LoginPage,
  ProfilePage,
  DashboardAdminPage,
  DashboardUserPage,
  DashboardPage,
  SearchPage,
  TabsPage,
  UsersCreatePage,
  UsersSearchPage,
  ListUsersPage,
  UserDetailPage,
  DeviceDetailPage,
  DevicesCreatePage,
  ReservePage,
  BookingPage,
  reservationAvailableDevicesPage,
  SearchReservePage,
  HistoryPage,
  HistoryDetailPage,
  HistorySearchPage
];

export function declarations() {
  return pages;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    Api,
    HistoryService,
    User,
    Devices,
    ReservationService,
    Display,
    Camera,
    SplashScreen,
    StatusBar,
    Network,
    NetworkService,
    Transfer,

    // Localisation
    { provide: LOCALE_ID, useValue: 'fr-fr' },

    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    BrowserModule,
    HttpModule,
    NgCalendarModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule { }
