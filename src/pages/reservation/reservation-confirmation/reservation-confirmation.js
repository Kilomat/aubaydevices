"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**
 * Generated class for the BookingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var BookingPage = (function () {
    function BookingPage(alertCtrl, nav, navParams, roomsService, user, historyService, loadingCtrl, translateS) {
        this.alertCtrl = alertCtrl;
        this.nav = nav;
        this.navParams = navParams;
        this.roomsService = roomsService;
        this.user = user;
        this.historyService = historyService;
        this.loadingCtrl = loadingCtrl;
        this.translateS = translateS;
        this.device = this.navParams.get('device');
        this.details = this.navParams.get('details');
        this.checkIn = this.details.from;
        this.checkOut = this.details.to;
    }
    /**
     * Popup reservation-confirmation. Booking a device .
     */
    BookingPage.prototype.PickupDevice = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('RESERVE_MESSAGE_PICKUP')['value'],
            buttons: [{
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                }, {
                    text: this.translateS.get('RESERVE')['value'],
                    handler: function () {
                        _this.book();
                    }
                }
            ]
        });
        alert.present();
    };
    BookingPage.prototype.book = function () {
        var _this = this;
        var newReservation = {
            _id: this.device._id,
            from: this.details.from,
            to: this.details.to,
            userId: this.user._user.idUser
        };
        var loading = this.loadingCtrl.create({
            content: "Booking device..."
        });
        loading.present();
        this.roomsService.reserveDevice(newReservation).then(function (res) {
            _this.addReservationToHistory(res);
            loading.dismiss();
            _this.nav.popToRoot();
        }, function (err) {
        });
    };
    BookingPage.prototype.addReservationToHistory = function (infos) {
        var data = {};
        data["idDevice"] = infos.idDevice;
        data["idUser"] = this.user._user.idUser;
        data["idReservation"] = infos.idReservation;
        data["timereturn"] = infos.to;
        data["timepickup"] = infos.from;
        data["token"] = this.user._user.token;
        var headers = new Headers({ 'x-access-token': this.user._user.token });
        this.historyService.createHistory(data, headers);
    };
    return BookingPage;
}());
BookingPage = __decorate([
    core_1.Component({
        selector: 'page-booking',
        templateUrl: 'reservation-confirmation.html',
    })
], BookingPage);
exports.BookingPage = BookingPage;
