import { Component } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {ReservationService} from "../../../providers/reservation";
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../../providers/user";

/**
 * Generated class for the BookingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-booking',
  templateUrl: 'reservation-confirmation.html',
})
export class BookingPage {

  device: any;
  details: any;
  checkIn: any;
  checkOut: any;

  constructor(private alertCtrl: AlertController, public nav: NavController, public navParams: NavParams,
              public reserveService: ReservationService, public user: User, public translateS: TranslateService) {
    this.device = this.navParams.get('device');
    this.details = this.navParams.get('details');
    this.checkIn = this.details.from;
    this.checkOut = this.details.to;
  }

  /**
   * Popup reservation-confirmation. Booking a device .
   */
  PickupDevice() {
    let alert = this.alertCtrl.create({
      message: this.translateS.get('RESERVE_MESSAGE_PICKUP')['value'],
      buttons: [{
        text: this.translateS.get('CANCEL_BUTTON')['value'],
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: this.translateS.get('RESERVE')['value'],
        handler: () => {
          this.bookingDevice();
        }
      }
      ]
    });
    alert.present();
  }

  bookingDevice(){
    let options = {
      _id: this.device._id,
      from: this.details.from,
      to: this.details.to,
      userId: this.user._user.idUser
    };

    this.reserveService.bookingTheDevice(options);
    this.nav.popToRoot();
  }
}
