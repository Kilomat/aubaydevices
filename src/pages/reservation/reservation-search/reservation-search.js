"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var reservation_available_devices_1 = require("../reservation-available-devices/reservation-available-devices");
var SearchReservePage = (function () {
    function SearchReservePage(nav, roomsService, alertCtrl, loadingCtrl) {
        this.nav = nav;
        this.roomsService = roomsService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        this.deviceType = 'smartphone';
        this.deviceOs = 'iOS';
        this.from = today.toISOString();
        this.to = tomorrow.toISOString();
    }
    SearchReservePage.prototype.findDevices = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Finding devices..."
        });
        loading.present();
        var options = {
            type: this.deviceType,
            os: this.deviceOs,
            from: this.from,
            to: this.to
        };
        this.roomsService.getAvailableDevices(options).then(function (data) {
            loading.dismiss();
            if (typeof (data[0]) === "undefined") {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: 'Sorry, no devices could be found for your search criteria.',
                    buttons: ['Ok']
                });
                alert_1.present();
            }
            else {
                _this.nav.push(reservation_available_devices_1.AvailableDevicesPage, {
                    devices: data,
                    details: options
                });
            }
        }, function (err) {
        });
    };
    return SearchReservePage;
}());
SearchReservePage = __decorate([
    core_1.Component({
        selector: 'page-search-reserve',
        templateUrl: 'reservation-search.html',
    })
], SearchReservePage);
exports.SearchReservePage = SearchReservePage;
