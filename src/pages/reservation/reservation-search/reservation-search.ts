import { Component } from '@angular/core';
import {AlertController, LoadingController, NavController} from 'ionic-angular';
import {ReservationService} from "../../../providers/reservation";
import {reservationAvailableDevicesPage} from "../reservation-available-devices/reservation-available-devices";
import moment from 'moment-timezone';

@Component({
  selector: 'page-search-reserve',
  templateUrl: 'reservation-search.html',
})
export class SearchReservePage {/*

  deviceType: any;
  deviceOs: any;
  from: any;
  to: any;

  constructor(public nav: NavController, public roomsService: ReservationService,
              public alertCtrl: AlertController, public loadingCtrl: LoadingController) {

    let today = new Date();
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    this.deviceType = 'smartphone';
    this.deviceOs = 'iOS';
    this.from = today.toISOString();
    this.to = tomorrow.toISOString();

  }

  findDevices(){

    let loading = this.loadingCtrl.create({
      content: "Finding devices..."
    });

    loading.present();

    let options = {
      type: this.deviceType,
      os: this.deviceOs,
      from: this.from,
      to: this.to
    };

    this.roomsService.getAvailableDevices(options).then((data) => {
      loading.dismiss();

      if(typeof(data[0]) === "undefined"){
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: 'Sorry, no devices could be found for your search criteria.',
          buttons: ['Ok']
        });
        alert.present();
      } else {
        this.nav.push(reservationAvailableDevicesPage, {
          devices: data,
          details: options
        });
      }
    }, (err) => {
    });

  }*/
}
