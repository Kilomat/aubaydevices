import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {BookingPage} from "../reservation-confirmation/reservation-confirmation";

@Component({
  selector: 'page-available-devices',
  templateUrl: 'reservation-available-devices.html',
})
export class reservationAvailableDevicesPage {
  devices: any;

  constructor(public nav: NavController, public navParams: NavParams) {
    this.devices = this.navParams.get('devices');
  }


  bookRoom(device){
    this.nav.push(BookingPage, {
      device: device,
      details: this.navParams.get('details')
    });
  }

}
