"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var reservation_confirmation_1 = require("../reservation-confirmation/reservation-confirmation");
var AvailableDevicesPage = (function () {
    function AvailableDevicesPage(nav, navParams) {
        this.nav = nav;
        this.navParams = navParams;
        this.devices = this.navParams.get('devices');
    }
    AvailableDevicesPage.prototype.bookRoom = function (device) {
        this.nav.push(reservation_confirmation_1.BookingPage, {
            device: device,
            details: this.navParams.get('details')
        });
    };
    return AvailableDevicesPage;
}());
AvailableDevicesPage = __decorate([
    core_1.Component({
        selector: 'page-available-devices',
        templateUrl: 'reservation-available-devices.html',
    })
], AvailableDevicesPage);
exports.AvailableDevicesPage = AvailableDevicesPage;
