import {Component} from '@angular/core';
import {AlertController, Platform} from 'ionic-angular';
import {NavController} from 'ionic-angular';
import {Devices} from '../../providers/providers';
import {User} from "../../providers/user";
import {HistoryService} from '../../providers/history';
import {TranslateService} from "@ngx-translate/core";
import {reservationAvailableDevicesPage} from "./reservation-available-devices/reservation-available-devices";
import {ReservationService} from "../../providers/reservation";
import * as moment from 'moment-timezone';
import {Display} from "../../providers/display";

@Component({
  selector: 'page-reserve',
  templateUrl: 'reservation.html'
})
export class ReservePage {
  deviceType: any;
  deviceOs: any;
  from: any;
  to: any;
  devices: any;
  userInfo: any;
  userReservations: any;
  allUserReservation = [];
  segment: string = "newdevice";
  isAndroid: boolean = false;

  constructor(private alertCtrl: AlertController, public nav: NavController, platform: Platform,
              public historyService: HistoryService, public user: User,
              public devicesService: Devices, public displayService: Display, public translateS: TranslateService,
              public reserviceS: ReservationService) {

    let today: String = moment().tz("Europe/Berlin").format();
    let tomorrow: String = moment().tz("Europe/Berlin").add(1, 'days').format();
    this.deviceType = 'smartphone';
    this.deviceOs = 'iOS';
    this.from = today;
    this.to = tomorrow;
    this.isAndroid = platform.is('android');
    this.displayService.showLoading();
    this.loadAllDevices(null);
  }


  /**
   * Delete an item from the list of items.
   */
  returnItem(item) {
    this.historyService.returnDevice(item.idreservation);
  }

  /**
   * Load all devices.
   */
  loadAllDevices(refresher) {
    this.devicesService.getAllDevices()
      .then(data => {
        this.devices = data;
        this.getUserInfos();
        this.displayService.loading.dismissAll();
        if (refresher)
        refresher.complete();

      });
  }


  /**
   * get user infos.
   */
  getUserInfos() {
    this.user.getuser(this.user._user.idUser)
      .then(data => {
        this.userInfo = data;
        if (data['histories']) {
         this.allUserReservation = [];
          this.userReservations = data['histories'];
          for (let k in this.userReservations) {
            let tmp : any = this.getDeviceInfo(this.userReservations[k]);
              this.allUserReservation.push(tmp);
          }
        }
      });
  }

  getDeviceInfo(id){
    for (let k in this.devices){
      let reserved : any = this.devices[k]["reserved"];
      if (reserved){
        for (let j in reserved){
          if (reserved[j]["_id"] == id){
            var obj = this.devices[k];
            var copie = Object.assign({}, obj);
            copie["to"] = reserved[j]["to"];
            copie["from"] = reserved[j]["from"];
            copie["idreservation"] = reserved[j]["_id"];
            return copie;
          }
        }
      }
    }
  }


  /**
   * Popup reservation-confirmation. Return a device .
   */
  ReturnDevice(item) {
    let alert = this.alertCtrl.create({
      message: this.translateS.get('RESERVE_MESSAGE_RETURN')['value'],
      buttons: [
        {
          text: this.translateS.get('CANCEL_BUTTON')['value'],
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: this.translateS.get('RETURN_DEVICE_BUTTON')['value'],
          handler: () => {
            this.returnItem(item);
          }
        }
      ]
    });
    alert.present();
  }


  /**
   * Looking for device.
   */
  findDevices(){
   this.displayService.showLoading();
    let options = {
      type: this.deviceType,
      os: this.deviceOs,
      from: this.from,
      to: this.to
    };

    this.reserviceS.getAvailableDevices(options).then((data) => {
      this.displayService.loading.dismiss();

      if(typeof(data[0]) === "undefined"){
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: this.translateS.get('ALERT_SEARCH_DEVICE')['value'],
          buttons: ['Ok']
        });
        alert.present();
      } else {
        this.nav.push(reservationAvailableDevicesPage, {
          devices: data,
          details: options
        });
      }
    }, (err) => {
    });
  }


  /**
   * Extend days .
   */
  ExtendDays(item){

    let alert = this.alertCtrl.create();
    alert.setTitle("Prolonger ma reservation jusqu'à...");

    alert.addInput({
      type: 'radio',
      label: 'Demain',
      value: moment(item["to"]).add(1, 'days').format(),
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Fin de matinée',
      value: moment(item["to"]).set({h: 12, m: 30}).format(),
    });

    alert.addInput({
      type: 'radio',
      label: 'Fin de journée',
      value: moment(item["to"]).set({h: 18, m: 30}).format(),
    });


    alert.addButton('Annuler');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log(data);
        this.reserviceS.availabilityChecking(item["_id"], item["to"], data, item)
      }
    });

    alert.present().then(() => {
    });
  }


/*  availabilityChecking(item, time){
    let loading = this.loadingCtrl.create({
      content: "Availability checking..."
    });

    loading.present();

    let options = {
      _id: item["_id"],
      from: item["to"],
      to: time,
      userId: this.user._user.idUser
    };

    this.reserviceS.checkAvailabilityDevice(options).then((data) => {
      loading.dismiss();

      if(typeof(data[0]) === "undefined"){
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: this.translateS.get('ALERT_SEARCH_DEVICE_ID')['value'],
          buttons: ['Ok']
        });
        alert.present();
      } else {
        item.to = options.to;
        this.bookingTheDevice(item);
      }
    }, (err) => {
    });
  }


  bookingTheDevice(item){
    this.devicesService.extendReservation(item);
    this.historyService.extendHistory(item);
  }*/

}
