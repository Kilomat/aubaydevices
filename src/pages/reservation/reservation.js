"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var reservation_search_1 = require("./reservation-search/reservation-search");
var reservation_available_devices_1 = require("./reservation-available-devices/reservation-available-devices");
var moment = require("moment-timezone");
var ReservePage = (function () {
    function ReservePage(alertCtrl, nav, items, toastRes, platform, historyService, user, devicesService, loadingCtrl, translateS, reserviceS) {
        this.alertCtrl = alertCtrl;
        this.nav = nav;
        this.items = items;
        this.toastRes = toastRes;
        this.historyService = historyService;
        this.user = user;
        this.devicesService = devicesService;
        this.loadingCtrl = loadingCtrl;
        this.translateS = translateS;
        this.reserviceS = reserviceS;
        this.allUserReservation = [];
        this.segment = "newdevice";
        this.isAndroid = false;
        var today = moment().tz("Europe/Berlin").format();
        var tomorrow = moment().tz("Europe/Berlin").add(1, 'days').format();
        this.deviceType = 'smartphone';
        this.deviceOs = 'iOS';
        this.from = today;
        this.to = tomorrow;
        this.isAndroid = platform.is('android');
        this.showLoading();
        this.loadAllDevices(null);
    }
    /**
     * Delete an item from the list of items.
     */
    ReservePage.prototype.returnItem = function (item) {
        this.historyService.returnDevice(item.idreservation);
    };
    /**
     * Load all devices.
     */
    ReservePage.prototype.loadAllDevices = function (refresher) {
        var _this = this;
        this.devicesService.getAllDevices()
            .then(function (data) {
            _this.devices = data;
            _this.getUserInfos();
            _this.loading.dismissAll();
            if (refresher)
                refresher.complete();
        });
    };
    /**
     * get user infos.
     */
    ReservePage.prototype.getUserInfos = function () {
        var _this = this;
        this.user.getuser(this.user._user.idUser)
            .then(function (data) {
            _this.userInfo = data;
            if (data['histories']) {
                _this.allUserReservation = [];
                _this.userReservations = data['histories'];
                for (var k in _this.userReservations) {
                    var tmp = _this.getDeviceInfo(_this.userReservations[k]);
                    _this.allUserReservation.push(tmp);
                }
            }
        });
    };
    ReservePage.prototype.getDeviceInfo = function (id) {
        for (var k in this.devices) {
            var reserved = this.devices[k]["reserved"];
            if (reserved) {
                for (var j in reserved) {
                    if (reserved[j]["_id"] == id) {
                        var obj = this.devices[k];
                        var copie = Object.assign({}, obj);
                        copie["to"] = reserved[j]["to"];
                        copie["from"] = reserved[j]["from"];
                        copie["idreservation"] = reserved[j]["_id"];
                        return copie;
                    }
                }
            }
        }
    };
    /**
     * Popup reservation-confirmation. Return a device .
     */
    ReservePage.prototype.ReturnDevice = function (item) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('RESERVE_MESSAGE_RETURN')['value'],
            buttons: [
                {
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: this.translateS.get('RETURN_DEVICE_BUTTON')['value'],
                    handler: function () {
                        _this.returnItem(item);
                    }
                }
            ]
        });
        alert.present();
    };
    ReservePage.prototype.showLoading = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        setTimeout(function () {
            _this.loading.dismiss();
        }, 5000);
    };
    ReservePage.prototype.findDevices = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Finding devices..."
        });
        loading.present();
        var options = {
            type: this.deviceType,
            os: this.deviceOs,
            from: this.from,
            to: this.to
        };
        this.reserviceS.getAvailableDevices(options).then(function (data) {
            loading.dismiss();
            if (typeof (data[0]) === "undefined") {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Oops!',
                    subTitle: _this.translateS.get('ALERT_SEARCH_DEVICE')['value'],
                    buttons: ['Ok']
                });
                alert_1.present();
            }
            else {
                _this.nav.push(reservation_available_devices_1.AvailableDevicesPage, {
                    devices: data,
                    details: options
                });
            }
        }, function (err) {
        });
    };
    ReservePage.prototype.openReservation = function () {
        this.nav.push(reservation_search_1.SearchReservePage);
    };
    return ReservePage;
}());
ReservePage = __decorate([
    core_1.Component({
        selector: 'page-reserve',
        templateUrl: 'reservation.html'
    })
], ReservePage);
exports.ReservePage = ReservePage;
