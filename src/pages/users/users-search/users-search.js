"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_detail_1 = require("../users-detail/user-detail");
var UsersSearchPage = (function () {
    function UsersSearchPage(navCtrl, navParams, user) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = user;
        this.loadDevices();
    }
    /**
     * Perform a service for the proper items.
     */
    UsersSearchPage.prototype.getItems = function (ev) {
        var val = ev.target.value;
        if (!val || !val.trim()) {
            this.users = [];
            return;
        }
        this.users = this.user.query({
            name: val,
            email: val,
            phone: val
        });
    };
    /**
     * get all users.
     */
    UsersSearchPage.prototype.loadDevices = function () {
        var _this = this;
        this.user.getAllUsers()
            .then(function (data) {
            _this.users = data;
        });
    };
    /**
     * Navigate to the detail page for this item.
     */
    UsersSearchPage.prototype.openItem = function (item) {
        this.navCtrl.push(user_detail_1.UserDetailPage, {
            item: item
        });
    };
    return UsersSearchPage;
}());
UsersSearchPage = __decorate([
    core_1.Component({
        selector: 'page-users-search',
        templateUrl: 'users-search.html',
    })
], UsersSearchPage);
exports.UsersSearchPage = UsersSearchPage;
