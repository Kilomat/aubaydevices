import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {UserDetailPage} from "../users-detail/user-detail";
import {User} from "../../../providers/user";
import {Item} from "../../../models/item";


@Component({
  selector: 'page-users-search',
  templateUrl: 'users-search.html',
})
export class UsersSearchPage {
  public users: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public user: User) {
    this.loadUser();
  }

  /**
   * Perform a service for the proper items.e
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.users = [];
      return;
    }
    this.users = this.user.query({
      name: val,
      email: val,
      phone: val
    });
  }

  /**
   * get all users.
   */
  loadUser(){
    this.user.getAllUsers()
      .then(data => {
        this.users = data;
      });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(UserDetailPage, {
      item: item
    });
  }

}
