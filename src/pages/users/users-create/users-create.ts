import {Component, ViewChild} from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {AlertController, NavController, ViewController} from 'ionic-angular';
import {Camera} from '@ionic-native/camera';
import {TranslateService} from "@ngx-translate/core";
import {InputValidators} from "./InputValidator";
import {User} from "../../../providers/user";
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import {Display} from "../../../providers/display";

@Component({
  selector: 'page-users-create',
  templateUrl: 'users-create.html'
})
export class UsersCreatePage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;
  item: any;
  form: FormGroup;

  constructor(public navCtrl: NavController,
              private alertCtrl: AlertController,
              public viewCtrl: ViewController,
              formBuilder: FormBuilder,
              public camera: Camera,
              private transfer: Transfer,
              public user: User,
              public toastRes: Display,
              public translateS: TranslateService) {

    this.form = formBuilder.group({
      picture: [''],
      name: ['', Validators.required],
      email: ['', [Validators.required, InputValidators.emailValidate()]],
      password: ['', [Validators.required, InputValidators.passwordValidate()]],
      status: [true],
      admin: [false],
      phone: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }


  ionViewDidLoad() {
  }

  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({'picture': 'data:image/jpg;base64,' + data});
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {
      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({'picture': imageData});
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['picture'].value + ')'
  }

  CreatNewUser(infos) {
    if (infos.status)
      infos.status = "1";
    else
      infos.status = "0";
    if (infos.admin)
      infos.admin = "1";
    else
      infos.admin = "0";
    // the picture is not available
    infos.picture = '';
    this.user.newUser(infos);
  }

  popupCreateUser() {
    let alert = this.alertCtrl.create({
      message: this.translateS.get('CREATE_NEW_USER_MESSAGE')['value'],
      buttons: [
        {
          text: this.translateS.get('CANCEL_BUTTON')['value'],
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: this.translateS.get('DONE_BUTTON')['value'],
          handler: () => {
            this.CreatNewUser(this.form.getRawValue());
          }
        }
      ]
    });
    alert.present();
  }

  hasError(field: string, error: string) {
    const ctrl = this.form.get(field);
    return ctrl.dirty && ctrl.hasError(error);
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) {
      return;
    }
    this.popupCreateUser();
    this.viewCtrl.dismiss(this.form.value);
  }
}
