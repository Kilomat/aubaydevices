/**
 * Created by hbaha on 04/08/2017.
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InputValidators = (function () {
    function InputValidators() {
    }
    InputValidators.passwordValidate = function () {
        return function (control) {
            if (!control.value || 0 === control.value.length) {
                return null;
            }
            if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$/.test(control.value)) {
                return null;
            }
            return { "passwordBadFormat": true };
        };
    };
    InputValidators.emailValidate = function () {
        return function (control) {
            if (!control.value || 0 === control.value.length) {
                return null;
            }
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(control.value)) {
                return null;
            }
            return { "emailBadFormat": true };
        };
    };
    return InputValidators;
}());
exports.InputValidators = InputValidators;
