"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var camera_1 = require("@ionic-native/camera");
var InputValidator_1 = require("./InputValidator");
var UsersCreatePage = (function () {
    function UsersCreatePage(navCtrl, alertCtrl, viewCtrl, formBuilder, camera, transfer, user, toastRes, translateS) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.user = user;
        this.toastRes = toastRes;
        this.translateS = translateS;
        this.form = formBuilder.group({
            picture: [''],
            name: ['', forms_1.Validators.required],
            email: ['', [forms_1.Validators.required, InputValidator_1.InputValidators.emailValidate()]],
            password: ['', [forms_1.Validators.required, InputValidator_1.InputValidators.passwordValidate()]],
            status: [true],
            admin: [false],
            phone: ['']
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (v) {
            _this.isReadyToSave = _this.form.valid;
        });
    }
    UsersCreatePage.prototype.ionViewDidLoad = function () {
    };
    UsersCreatePage.prototype.getPicture = function () {
        var _this = this;
        if (camera_1.Camera['installed']()) {
            this.camera.getPicture({
                destinationType: this.camera.DestinationType.DATA_URL,
                targetWidth: 96,
                targetHeight: 96
            }).then(function (data) {
                _this.form.patchValue({ 'picture': 'data:image/jpg;base64,' + data });
            }, function (err) {
                alert('Unable to take photo');
            });
        }
        else {
            this.fileInput.nativeElement.click();
        }
    };
    UsersCreatePage.prototype.processWebImage = function (event) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var imageData = readerEvent.target.result;
            _this.form.patchValue({ 'picture': imageData });
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    UsersCreatePage.prototype.getProfileImageStyle = function () {
        return 'url(' + this.form.controls['picture'].value + ')';
    };
    UsersCreatePage.prototype.CreatNewUser = function (infos) {
        if (infos.status)
            infos.status = "1";
        else
            infos.status = "0";
        if (infos.admin)
            infos.admin = "1";
        else
            infos.admin = "0";
        // the picture is not available
        infos.picture = '';
        this.user.newUser(infos);
    };
    UsersCreatePage.prototype.popupCreateUser = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('CREATE_NEW_USER_MESSAGE')['value'],
            buttons: [
                {
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: this.translateS.get('DONE_BUTTON')['value'],
                    handler: function () {
                        _this.CreatNewUser(_this.form.getRawValue());
                    }
                }
            ]
        });
        alert.present();
    };
    UsersCreatePage.prototype.hasError = function (field, error) {
        var ctrl = this.form.get(field);
        return ctrl.dirty && ctrl.hasError(error);
    };
    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    UsersCreatePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    UsersCreatePage.prototype.done = function () {
        if (!this.form.valid) {
            return;
        }
        this.popupCreateUser();
        this.viewCtrl.dismiss(this.form.value);
    };
    return UsersCreatePage;
}());
__decorate([
    core_1.ViewChild('fileInput')
], UsersCreatePage.prototype, "fileInput", void 0);
UsersCreatePage = __decorate([
    core_1.Component({
        selector: 'page-users-create',
        templateUrl: 'users-create.html'
    })
], UsersCreatePage);
exports.UsersCreatePage = UsersCreatePage;
