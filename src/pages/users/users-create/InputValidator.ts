/**
 * Created by hbaha on 04/08/2017.
 */

import { AbstractControl} from "@angular/forms";

export class InputValidators {

  static passwordValidate() {
    return (control:AbstractControl): {[key:string] : boolean} => {
      if (!control.value || 0 === control.value.length) {
        return null;
      }

      if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,12}$/.test(control.value)){
        return null;
      }

      return {"passwordBadFormat": true};
    };
  }

  static emailValidate() {
    return (control:AbstractControl): {[key:string] : boolean} => {
      if (!control.value || 0 === control.value.length) {
        return null;
      }

      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(control.value)){
        return null;
      }

      return {"emailBadFormat": true};
    };
  }
}
