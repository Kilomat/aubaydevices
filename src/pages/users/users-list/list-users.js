"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var users_search_1 = require("../users-search/users-search");
var users_create_1 = require("../users-create/users-create");
var user_detail_1 = require("../users-detail/user-detail");
var ListUsersPage = (function () {
    function ListUsersPage(navCtrl, alertCtrl, modalCtrl, user, translateS) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.user = user;
        this.translateS = translateS;
        this.loadUsers();
    }
    /**
     * The view loaded, let's query our items for the list
     */
    ListUsersPage.prototype.ionViewDidLoad = function () {
    };
    /**
     * Navigate to the user create page.
     */
    ListUsersPage.prototype.addItem = function () {
        this.navCtrl.push(users_create_1.UsersCreatePage);
    };
    ListUsersPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.user.getAllUsers()
            .then(function (data) {
            _this.users = data;
            refresher.complete();
        });
    };
    /**
     * Delete an item from the list of items.
     */
    ListUsersPage.prototype.deleteItem = function (item) {
        this.user.deleteUser(item._id);
        //this.items.delete(item);
    };
    /**
     * Enable or disable user account.
     */
    ListUsersPage.prototype.suspendItem = function (item) {
        if (item.status)
            item.status = "0";
        else
            item.status = "1";
        this.user.changeUser(item);
        //this.items.change(item);
    };
    /**
     * get all users.
     */
    ListUsersPage.prototype.loadUsers = function () {
        var _this = this;
        this.user.getAllUsers()
            .then(function (data) {
            _this.users = data;
        });
    };
    /**
     * Navigate to the detail page for this item.
     */
    ListUsersPage.prototype.openItem = function (item) {
        this.navCtrl.push(user_detail_1.UserDetailPage, {
            item: item
        });
    };
    /**
     * Navigate to the search page.
     */
    ListUsersPage.prototype.opensearch = function () {
        this.navCtrl.push(users_search_1.UsersSearchPage);
    };
    ListUsersPage.prototype.popupChangeStatusUser = function (item) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('STATUS_CHANGE_USER_MESSAGE')['value'],
            buttons: [{
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                }, {
                    text: this.translateS.get('DONE_BUTTON')['value'],
                    handler: function () {
                        _this.suspendItem(item);
                    }
                }
            ]
        });
        alert.present();
    };
    ListUsersPage.prototype.popupDeleteUser = function (item) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('DELETE_USER_MESSAGE')['value'],
            buttons: [{
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                }, {
                    text: this.translateS.get('DONE_BUTTON')['value'],
                    handler: function () {
                        _this.deleteItem(item);
                    }
                }
            ]
        });
        alert.present();
    };
    return ListUsersPage;
}());
ListUsersPage = __decorate([
    core_1.Component({
        selector: 'page-list-users',
        templateUrl: 'list-users.html',
    })
], ListUsersPage);
exports.ListUsersPage = ListUsersPage;
