import {Component} from '@angular/core';
import {NavController, ModalController, AlertController} from 'ionic-angular';
import {UsersSearchPage} from "../users-search/users-search";
import {UsersCreatePage} from "../users-create/users-create";
import {UserDetailPage} from "../users-detail/user-detail";
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../../providers/user";
import {Item} from "../../../models/item";

@Component({
  selector: 'page-list-users',
  templateUrl: 'list-users.html',
})
export class ListUsersPage {
  public users: any;

  constructor(public navCtrl: NavController,  private alertCtrl: AlertController,
              public modalCtrl: ModalController, public user: User, public translateS:TranslateService) {
    this.loadUsers();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Navigate to the user create page.
   */
  addItem(){
    this.navCtrl.push(UsersCreatePage);
  }

  doRefresh(refresher) {
    this.user.getAllUsers()
      .then(data => {
        this.users = data;
        refresher.complete();
      });
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.user.deleteUser(item._id);
    //this.items.delete(item);
  }

  /**
   * Enable or disable user account.
   */
  suspendItem(item) {
    if (item.status)
      item.status = "0";
    else
      item.status = "1";
    this.user.changeUser(item);
    //this.items.change(item);
  }

  /**
   * get all users.
   */
  loadUsers(){
    this.user.getAllUsers()
      .then(data => {
        this.users = data;
      })
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(UserDetailPage, {
      item: item
    });
  }

  /**
   * Navigate to the search page.
   */
  opensearch(){
    this.navCtrl.push(UsersSearchPage);
  }

  popupChangeStatusUser(item) {

    let alert = this.alertCtrl.create({
      message: this.translateS.get('STATUS_CHANGE_USER_MESSAGE')['value'],
      buttons: [{
        text: this.translateS.get('CANCEL_BUTTON')['value'],
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: this.translateS.get('DONE_BUTTON')['value'],
        handler: () => {
          this.suspendItem(item);
        }
      }
      ]
    });
    alert.present();
  }

  popupDeleteUser(item) {

    let alert = this.alertCtrl.create({
      message: this.translateS.get('DELETE_USER_MESSAGE')['value'],
      buttons: [{
        text: this.translateS.get('CANCEL_BUTTON')['value'],
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: this.translateS.get('DONE_BUTTON')['value'],
        handler: () => {
          this.deleteItem(item);
        }
      }
      ]
    });
    alert.present();
  }
}
