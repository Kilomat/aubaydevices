/**
 * Created by hbaha on 24/05/2017.
 */

import { Component } from '@angular/core';
import {AlertController, NavController, NavParams, ViewController} from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {User} from "../../../providers/user";


@Component({
  selector: 'page-users-detail',
  templateUrl: 'user-detail.html'
})
export class UserDetailPage {
  item: any;
  userActive:any;
  userInactive:any;
  isReadyToSave: boolean;
  form: FormGroup;

  constructor(public navCtrl: NavController, navParams: NavParams,
              public translateS: TranslateService, formBuilder: FormBuilder,
              public user: User, public viewCtrl: ViewController,
              private alertCtrl: AlertController) {
    this.userActive = this.translateS.get('USER_STATUS_ACTIVE')['value'];
    this.userInactive = this.translateS.get('USER_STATUS_INACTIVE')['value'];
    this.item = navParams.get('item');

    this.form = formBuilder.group({
      name: [''],
      email: [''],
      phone: [''],
      status:[''],
      picture:[''],
      admin:[''],
      _id: ['']
    });

    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.form.value['_id'] = this.item["_id"];
    this.form.value['status'] = this.form.value['status'] ? "1" : "0";
    this.form.value['admin'] = this.form.value['admin'] ? "1" : "0";
    this.user.changeUser(this.form.value);
    this.viewCtrl.dismiss(this.form.value);
  }


  hasError(field: string, error: string) {
    const ctrl = this.form.get(field);
    return ctrl.dirty && ctrl.hasError(error);
  }

  popupChangeProfile() {

    let alert = this.alertCtrl.create({
      message: this.translateS.get('CHANGE_PROFILE_MESSAGE')['value'],
      buttons: [{
        text: this.translateS.get('CANCEL_BUTTON')['value'],
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: this.translateS.get('DONE_BUTTON')['value'],
        handler: () => {
          this.done();
        }
      }
      ]
    });
    alert.present();
  }
}
