/**
 * Created by hbaha on 29/05/2017.
 */

import {Component} from '@angular/core';
import {ModalController, NavController} from "ionic-angular";
import {HistoryService} from '../../providers/history';
import {Devices} from "../../providers/devices";
import {User} from "../../providers/user";
import {HistorySearchPage} from "./history-search/history-search";
import {Item} from "../../models/item";
import {HistoryDetailPage} from "./history-detail/history-detail";
import * as moment from 'moment-timezone';
import * as _ from 'lodash';
import {Display} from "../../providers/display";

@Component({
  selector: 'page-history',
  templateUrl: 'history.html'
})
export class HistoryPage {
  allHistory: any[];
  history: any;
  dateFilter: string;
  devicedata: any;
  userdata: any;
  useDateFilter = false;
  uselastReservation: false;


  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public devicesService: Devices,
              public user: User, public displayService: Display, public historyService: HistoryService) {
    this.displayService.showLoading();
    this.dateFilter = moment().tz("Europe/Berlin").format();
    this.load(null);
  }

  load(refresher){
  this.loadAllUsers();
  this.loadAllDevices(refresher);
}

  loadHistory(refresher) {
    this.historyService.getHistories()
      .then(data => {
        for (let s in data) {
          data[s] = this.getDeviceInfos(data[s]);
        }
        this.history = data;
        this.allHistory = this.history;
        this.displayService.loading.dismissAll();
        if (refresher)
          refresher.complete();
      });
  }

  /**
   * get all devices.
   */
  loadAllDevices(refresher) {
    this.devicesService.getAllDevices()
      .then(data => {
        this.devicedata = data;
        this.loadHistory(refresher);
      });
  }

  /**
   * get all users.
   */
  loadAllUsers() {
    this.user.getAllUsers()
      .then(data => {
        this.userdata = data;
      });
  }

  /**
   * Load device.
   */
  getDeviceInfos(device) {

    for (let k in this.devicedata) {
      if (this.devicedata[k]['_id'] == device.idDevice) {
        device["picture"] = this.devicedata[k]['picture'];
        device["brand"] = this.devicedata[k]['brand'];
        device["name"] = this.devicedata[k]['name'];
        var obj = device;
        var copie = Object.assign({}, obj);
        copie = this.getUserInfos(copie);
        return copie;
      }
    }
    return device;
  }

  /**
   * Load device.
   */
  getUserInfos(infos) {
    for (let k in this.userdata) {
      if (this.userdata[k]['_id'] == infos.idUser) {
        infos["userphone"] = this.userdata[k]['phone'];
        infos["useremail"] = this.userdata[k]['email'];
        infos["username"] = this.userdata[k]['name'];
        return infos;
      }
    }
    return infos;
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }


  opensearch(item: Item) {
    this.navCtrl.push(HistorySearchPage, {
      item: item
    });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(HistoryDetailPage, {
      item: item
    });
  }

  dateChanged() {
    this.uselastReservation = false;
    if (this.useDateFilter)
      this.history = _.filter(this.allHistory, g => moment(g.timepickup).tz("Europe/Berlin").isSame(this.dateFilter, 'day'));
     else
      this.history = this.allHistory;
  }

  lastReservations() {
    this.useDateFilter = false;
    if (this.uselastReservation)
        this.history = _.slice(this.allHistory, 0, 10);
    else
      this.history = this.allHistory;
  }

}
