"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var history_detail_1 = require("../history-detail/history-detail");
var HistorySearchPage = (function () {
    function HistorySearchPage(navCtrl, navParams, devicesService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.devicesService = devicesService;
        this.devices = navParams.get('item');
        this.searchdata = this.devices;
    }
    /**
     * Perform a service for the proper items.
     */
    HistorySearchPage.prototype.getItems = function (ev) {
        var val = ev.target.value;
        if (!val || !val.trim()) {
            this.devices = [];
            return;
        }
        this.devices = this.query({
            name: val,
            brand: val,
            username: val,
            useremail: val,
            type: val
        });
    };
    /**
      * Process a users response to search user
    */
    HistorySearchPage.prototype.query = function (params) {
        if (!params)
            return this.searchdata;
        return this.searchdata.filter(function (item) {
            for (var key in params) {
                var field = item[key];
                if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
                    return item;
                }
                else if (field == params[key]) {
                    return item;
                }
            }
            return null;
        });
    };
    /**
     * Navigate to the detail page for this item.
     */
    HistorySearchPage.prototype.openItem = function (item) {
        this.navCtrl.push(history_detail_1.HistoryDetailPage, {
            item: item
        });
    };
    return HistorySearchPage;
}());
HistorySearchPage = __decorate([
    core_1.Component({
        selector: 'page-history-search',
        templateUrl: 'history-search.html',
    })
], HistorySearchPage);
exports.HistorySearchPage = HistorySearchPage;
