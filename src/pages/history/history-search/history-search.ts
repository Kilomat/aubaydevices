import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {Devices} from "../../../providers/devices";
import {Item} from "../../../models/item";
import {HistoryDetailPage} from "../history-detail/history-detail";

@Component({
  selector: 'page-history-search',
  templateUrl: 'history-search.html',
})
export class HistorySearchPage {

  devices: any;
  searchdata: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public devicesService: Devices) {
    this.devices = navParams.get('item');
    this.searchdata = this.devices;
  }

  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.devices = [];
      return;
    }
    this.devices = this.query({
      name: val,
      brand: val,
      username: val,
      useremail:val,
      type: val
    });
  }

  /**
    * Process a users response to search user
  */

  query(params?: any) {
    if (!params)
      return this.searchdata;

    return this.searchdata.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(HistoryDetailPage, {
      item: item
    });
  }

}
