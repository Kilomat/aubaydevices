/**
 * Created by hbaha on 29/05/2017.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var history_search_1 = require("./history-search/history-search");
var history_detail_1 = require("./history-detail/history-detail");
var moment = require("moment-timezone");
var _ = require("lodash");
var HistoryPage = (function () {
    function HistoryPage(navCtrl, modalCtrl, devicesService, user, loadingCtrl, historyService) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.devicesService = devicesService;
        this.user = user;
        this.loadingCtrl = loadingCtrl;
        this.historyService = historyService;
        this.useDateFilter = false;
        this.showLoading();
        this.dateFilter = moment().tz("Europe/Berlin").format();
        this.load(null);
    }
    HistoryPage.prototype.load = function (refresher) {
        this.loadAllUsers();
        this.loadAllDevices(refresher);
    };
    HistoryPage.prototype.loadHistory = function (refresher) {
        var _this = this;
        this.historyService.getHistories()
            .then(function (data) {
            for (var s in data) {
                data[s] = _this.getDeviceInfos(data[s]);
            }
            _this.history = data;
            _this.allHistory = _this.history;
            _this.loading.dismissAll();
            if (refresher)
                refresher.complete();
        });
    };
    /**
     * get all devices.
     */
    HistoryPage.prototype.loadAllDevices = function (refresher) {
        var _this = this;
        this.devicesService.getAllDevices()
            .then(function (data) {
            _this.devicedata = data;
            _this.loadHistory(refresher);
        });
    };
    /**
     * get all users.
     */
    HistoryPage.prototype.loadAllUsers = function () {
        var _this = this;
        this.user.getAllUsers()
            .then(function (data) {
            _this.userdata = data;
        });
    };
    /**
     * Load device.
     */
    HistoryPage.prototype.getDeviceInfos = function (device) {
        for (var k in this.devicedata) {
            if (this.devicedata[k]['_id'] == device.idDevice) {
                device["picture"] = this.devicedata[k]['picture'];
                device["brand"] = this.devicedata[k]['brand'];
                device["name"] = this.devicedata[k]['name'];
                var obj = device;
                var copie = Object.assign({}, obj);
                copie = this.getUserInfos(copie);
                return copie;
            }
        }
        return device;
    };
    /**
     * Load device.
     */
    HistoryPage.prototype.getUserInfos = function (infos) {
        for (var k in this.userdata) {
            if (this.userdata[k]['_id'] == infos.idUser) {
                infos["userphone"] = this.userdata[k]['phone'];
                infos["useremail"] = this.userdata[k]['email'];
                infos["username"] = this.userdata[k]['name'];
                return infos;
            }
        }
        return infos;
    };
    /**
     * The view loaded, let's query our items for the list
     */
    HistoryPage.prototype.ionViewDidLoad = function () {
    };
    HistoryPage.prototype.showLoading = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        setTimeout(function () {
            _this.loading.dismiss();
        }, 10000);
    };
    HistoryPage.prototype.opensearch = function (item) {
        this.navCtrl.push(history_search_1.HistorySearchPage, {
            item: item
        });
    };
    /**
     * Navigate to the detail page for this item.
     */
    HistoryPage.prototype.openItem = function (item) {
        this.navCtrl.push(history_detail_1.HistoryDetailPage, {
            item: item
        });
    };
    HistoryPage.prototype.dateChanged = function () {
        var _this = this;
        this.uselastReservation = false;
        if (this.useDateFilter)
            this.history = _.filter(this.allHistory, function (g) { return moment(g.timepickup).tz("Europe/Berlin").isSame(_this.dateFilter, 'day'); });
        else
            this.history = this.allHistory;
    };
    HistoryPage.prototype.lastReservations = function () {
        this.useDateFilter = false;
        if (this.uselastReservation)
            this.history = _.slice(this.allHistory, 0, 10);
        else
            this.history = this.allHistory;
    };
    return HistoryPage;
}());
HistoryPage = __decorate([
    core_1.Component({
        selector: 'page-history',
        templateUrl: 'history.html'
    })
], HistoryPage);
exports.HistoryPage = HistoryPage;
