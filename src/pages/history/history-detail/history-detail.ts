/**
 * Created by hbaha on 24/05/2017.
 */

import { Component } from '@angular/core';
import { NavParams} from 'ionic-angular';
import {ReservationService} from "../../../providers/reservation";
import * as moment from 'moment-timezone';

@Component({
  selector: 'page-history-detail',
  templateUrl: 'history-detail.html'
})
export class HistoryDetailPage {
  device: any;
  from: any;
  to: any;

  constructor(navParams: NavParams, public reserviceService: ReservationService) {

    let today: String = moment().tz("Europe/Berlin").format();
    let tomorrow: String = moment().tz("Europe/Berlin").add(1, 'days').format();
    this.from = today;
    this.to = tomorrow;
    this.device = navParams.get('item');
  }

  availabilityChecking(){
    this.reserviceService.availabilityChecking(this.device["idDevice"], this.from, this.to, null);
  }

/*  availabilityChecking(){
    let loading = this.loadingCtrl.create({
      content: "Availability checking..."
    });

    loading.present();

    let options = {
      _id: this.device["idDevice"],
      from: this.from,
      to: this.to,
      userId: this.user._user.idUser
    };

    this.reserviceService.checkAvailabilityDevice(options).then((data) => {
      loading.dismiss();

      if(typeof(data[0]) === "undefined"){
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: this.translateS.get('ALERT_SEARCH_DEVICE_ID')['value'],
          buttons: ['Ok']
        });
        alert.present();
      } else {
        this.bookingTheDevice(options);
      }
    }, (err) => {
    });
  }

  bookingTheDevice(options){
    let loading = this.loadingCtrl.create({
      content: "Booking device..."
    });

    loading.present();

    this.reserviceService.reserveDevice(options).then((res) => {
      this.addReservationToHistory(res);
      loading.dismiss();
    }, (err) => {
    });
  }

  addReservationToHistory(infos){
    let data = {};
    data["idDevice"] = infos.idDevice;
    data["idUser"] = this.user._user.idUser;
    data["idReservation"] = infos.idReservation;
    data["timereturn"] = infos.to;
    data["timepickup"] = infos.from;
    data["token"] = this.user._user.token;
    let headers = new Headers({'x-access-token': this.user._user.token});
    this.historyService.createHistory(data, headers);
  }*/
}
