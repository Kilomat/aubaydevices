/**
 * Created by hbaha on 24/05/2017.
 */

import { Component } from '@angular/core';
import {Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import * as moment from 'moment-timezone';
import {ReservationService} from "../../../providers/reservation";


@Component({
  selector: 'page-device-detail',
  templateUrl: 'device-detail.html'
})
export class DeviceDetailPage {
  device: any;
  loading: Loading;
  eventSource;
  viewTitle;
  noEventsLabel;
  from: any;
  to: any;
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    startingDayMonth : 1,
    dateFormatter: {
      formatMonthViewDay: function(date:Date) {
        return date.getDate().toString();
      }
    }
  };

  constructor(navParams: NavParams, public navCtrl: NavController, public reserviceService: ReservationService) {

    this.device = navParams.get('item');
    this.loadEvents();
    let today: String = moment().tz("Europe/Berlin").format();
    let tomorrow: String = moment().tz("Europe/Berlin").add(1, 'days').format();
    this.from = today;
    this.to = tomorrow;
  }


  loadEvents() {
    this.eventSource = this.createEvents();
    this.noEventsLabel = "Aucune réservation.";
  }

  changeMode(mode) {
    this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onCurrentDateChanged(event:Date) {
    event.setHours(0, 0, 0, 0);
    this.viewTitle = event.getTime();
  }

  createEvents(){
    var events = [];
    var reservation = this.device.currentReservation;
    for (var i in reservation) {
      events.push({
        title: reservation[i]["email"] + " " + reservation[i]["name"],
        startTime: moment(reservation[i]["from"]).toDate(),
        endTime: moment(reservation[i]["to"]).toDate(),
        allDay: false
      });
    }
    return events;
  }

  availabilityChecking(){
    this.reserviceService.availabilityChecking(this.device["_id"], this.from, this.to, null);
  }

/*  availabilityChecking(){
    let loading = this.loadingCtrl.create({
      content: "Availability checking..."
    });

    loading.present();

    let options = {
      _id: this.device["_id"],
      from: this.from,
      to: this.to,
      userId: this.user._user.idUser
    };

    this.reserviceService.checkAvailabilityDevice(options).then((data) => {
      loading.dismiss();

      if(typeof(data[0]) === "undefined"){
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: this.translateS.get('ALERT_SEARCH_DEVICE_ID')['value'],
          buttons: ['Ok']
        });
        alert.present();
      } else {
        this.bookingTheDevice(options);
      }
    }, (err) => {
    });
  }

  bookingTheDevice(options){
    let loading = this.loadingCtrl.create({
      content: "Booking device..."
    });

    loading.present();

    this.reserviceService.reserveDevice(options).then((res) => {
      this.addReservationToHistory(res);
      loading.dismiss();
    }, (err) => {
    });
  }

  addReservationToHistory(infos){
    let data = {};
    data["idDevice"] = infos.idDevice;
    data["idUser"] = this.user._user.idUser;
    data["idReservation"] = infos.idReservation;
    data["timereturn"] = infos.to;
    data["timepickup"] = infos.from;
    data["token"] = this.user._user.token;
    let headers = new Headers({'x-access-token': this.user._user.token});
    this.historyService.createHistory(data, headers);
  }*/

}
