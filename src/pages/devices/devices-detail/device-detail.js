/**
 * Created by hbaha on 24/05/2017.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var moment = require("moment-timezone");
var DeviceDetailPage = (function () {
    function DeviceDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.calendar = {
            mode: 'month',
            currentDate: new Date(),
            dateFormatter: {
                formatMonthViewDay: function (date) {
                    return date.getDate().toString();
                }
            }
        };
        this.markDisabled = function (date) {
            var current = new Date();
            current.setHours(0, 0, 0);
            return date < current;
        };
        this.device = navParams.get('item');
        this.loadEvents();
    }
    DeviceDetailPage.prototype.loadEvents = function () {
        this.eventSource = this.createEvents();
        this.noEventsLabel = "Aucune réservation.";
    };
    DeviceDetailPage.prototype.changeMode = function (mode) {
        this.calendar.mode = mode;
    };
    DeviceDetailPage.prototype.today = function () {
        this.calendar.currentDate = new Date();
    };
    DeviceDetailPage.prototype.onCurrentDateChanged = function (event) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    };
    DeviceDetailPage.prototype.createEvents = function () {
        var events = [];
        var reservation = this.device.currentReservation;
        for (var i in reservation) {
            events.push({
                title: reservation[i]["email"] + " " + reservation[i]["name"],
                startTime: moment(reservation[i]["from"]).toDate(),
                endTime: moment(reservation[i]["to"]).toDate(),
                allDay: false
            });
        }
        return events;
    };
    return DeviceDetailPage;
}());
DeviceDetailPage = __decorate([
    core_1.Component({
        selector: 'page-device-detail',
        templateUrl: 'device-detail.html'
    })
], DeviceDetailPage);
exports.DeviceDetailPage = DeviceDetailPage;
