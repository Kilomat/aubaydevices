import {Component, ViewChild} from '@angular/core';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {AlertController, NavController, ViewController} from 'ionic-angular';
import {Camera} from '@ionic-native/camera';
import {TranslateService} from "@ngx-translate/core";
import {Devices} from "../../../providers/devices";
import {User} from "../../../providers/user";



@Component({
  selector: 'page-devices-create',
  templateUrl: 'devices-create.html'
})
export class DevicesCreatePage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;
  item: any;
  form: FormGroup;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public viewCtrl: ViewController,
              formBuilder: FormBuilder, public camera: Camera, public device: Devices,
              public user: User, public translateS:TranslateService) {

    this.form = formBuilder.group({
      picture: [''],
      IdFactory: ['', Validators.required],
      name: ['', Validators.required],
      brand: ['', Validators.required],
      os: ['', Validators.required],
      type: ['', Validators.required],
      height: [null, Validators.required],
      width: [null, Validators.required],
      screen: [null, Validators.required],
      density: [''],
      version: ['', Validators.required],
      //token: this.user._user.token,
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((value) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  onChange($event) {
    let height = this.form.controls['height'].value;
    let width = this.form.controls['width'].value;
    let screen = this.form.controls['screen'].value;
    if (height && width && screen) {
      this.getPixalDensity(height, width, screen);
    }
  }

  ionViewDidLoad() {
  }

  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({'picture': 'data:image/jpg;base64,' + data});
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {
      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({'picture': imageData});
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['picture'].value + ')'
  }

  getPixalDensity(height, width, screen) {
    let dP = Math.sqrt((Math.pow(width, 2) + Math.pow(height, 2)));
    let PPI = Math.round(dP / screen);
    this.form.controls['density'].setValue(PPI);
  }

  CreatNewDevice(infos) {
    // the picture is not available
    infos.picture = '';
    this.device.newDevice(infos);
  }

  popupCreateDevice() {

    let alert = this.alertCtrl.create({
      message: this.translateS.get('CREATE_NEW_DEVICE_MESSAGE')['value'],
      buttons: [{
          text: this.translateS.get('CANCEL_BUTTON')['value'],
          role: 'cancel',
          handler: () => {
          }
        }, {
          text: this.translateS.get('DONE_BUTTON')['value'],
          handler: () => {
            this.CreatNewDevice(this.form.getRawValue());
          }
        }
      ]
    });
    alert.present();
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) {
      return;
    }
    this.popupCreateDevice();
    this.viewCtrl.dismiss(this.form.value);
  }
}
