"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var camera_1 = require("@ionic-native/camera");
var DevicesCreatePage = (function () {
    function DevicesCreatePage(navCtrl, alertCtrl, viewCtrl, formBuilder, camera, device, user, translateS) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.camera = camera;
        this.device = device;
        this.user = user;
        this.translateS = translateS;
        this.form = formBuilder.group({
            picture: [''],
            IdFactory: ['', forms_1.Validators.required],
            name: ['', forms_1.Validators.required],
            brand: ['', forms_1.Validators.required],
            os: ['', forms_1.Validators.required],
            type: ['', forms_1.Validators.required],
            height: [null, forms_1.Validators.required],
            width: [null, forms_1.Validators.required],
            screen: [null, forms_1.Validators.required],
            density: [''],
            version: ['', forms_1.Validators.required],
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (value) {
            _this.isReadyToSave = _this.form.valid;
        });
    }
    DevicesCreatePage.prototype.onChange = function ($event) {
        var height = this.form.controls['height'].value;
        var width = this.form.controls['width'].value;
        var screen = this.form.controls['screen'].value;
        if (height && width && screen) {
            this.getPixalDensity(height, width, screen);
        }
    };
    DevicesCreatePage.prototype.ionViewDidLoad = function () {
    };
    DevicesCreatePage.prototype.getPicture = function () {
        var _this = this;
        if (camera_1.Camera['installed']()) {
            this.camera.getPicture({
                destinationType: this.camera.DestinationType.DATA_URL,
                targetWidth: 96,
                targetHeight: 96
            }).then(function (data) {
                _this.form.patchValue({ 'picture': 'data:image/jpg;base64,' + data });
            }, function (err) {
                alert('Unable to take photo');
            });
        }
        else {
            this.fileInput.nativeElement.click();
        }
    };
    DevicesCreatePage.prototype.processWebImage = function (event) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var imageData = readerEvent.target.result;
            _this.form.patchValue({ 'picture': imageData });
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    DevicesCreatePage.prototype.getProfileImageStyle = function () {
        return 'url(' + this.form.controls['picture'].value + ')';
    };
    DevicesCreatePage.prototype.getPixalDensity = function (height, width, screen) {
        var dP = Math.sqrt((Math.pow(width, 2) + Math.pow(height, 2)));
        var PPI = Math.round(dP / screen);
        this.form.controls['density'].setValue(PPI);
    };
    DevicesCreatePage.prototype.CreatNewDevice = function (infos) {
        // the picture is not available
        infos.picture = '';
        this.device.newDevice(infos);
    };
    DevicesCreatePage.prototype.popupCreateDevice = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('CREATE_NEW_DEVICE_MESSAGE')['value'],
            buttons: [{
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                }, {
                    text: this.translateS.get('DONE_BUTTON')['value'],
                    handler: function () {
                        _this.CreatNewDevice(_this.form.getRawValue());
                    }
                }
            ]
        });
        alert.present();
    };
    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    DevicesCreatePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    DevicesCreatePage.prototype.done = function () {
        if (!this.form.valid) {
            return;
        }
        this.popupCreateDevice();
        this.viewCtrl.dismiss(this.form.value);
    };
    return DevicesCreatePage;
}());
__decorate([
    core_1.ViewChild('fileInput')
], DevicesCreatePage.prototype, "fileInput", void 0);
DevicesCreatePage = __decorate([
    core_1.Component({
        selector: 'page-devices-create',
        templateUrl: 'devices-create.html'
    })
], DevicesCreatePage);
exports.DevicesCreatePage = DevicesCreatePage;
