import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DeviceDetailPage } from '../devices-detail/device-detail';
import {Devices} from "../../../providers/devices";
import {Item} from "../../../models/item";


@Component({
  selector: 'page-search',
  templateUrl: 'devices-search.html',
})
export class SearchPage {

  public devices: any;

  constructor(public navCtrl: NavController, public devicesService: Devices) {
    this.loadDevices();
  }

  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.devices = [];
      return;
    }
    this.devices = this.devicesService.query({
      name: val,
      brand: val,
      type: val
    });
  }

  loadDevices(){
    this.devicesService.getAllDevices()
      .then(data => {
        this.devices = data;
      });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(DeviceDetailPage, {
      item: item
    });
  }

}
