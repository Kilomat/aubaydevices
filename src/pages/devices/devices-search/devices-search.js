"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var device_detail_1 = require("../devices-detail/device-detail");
var SearchPage = (function () {
    function SearchPage(navCtrl, devicesService) {
        this.navCtrl = navCtrl;
        this.devicesService = devicesService;
        this.loadDevices();
    }
    /**
     * Perform a service for the proper items.
     */
    SearchPage.prototype.getItems = function (ev) {
        var val = ev.target.value;
        if (!val || !val.trim()) {
            this.devices = [];
            return;
        }
        this.devices = this.devicesService.query({
            name: val,
            brand: val,
            type: val
        });
    };
    SearchPage.prototype.loadDevices = function () {
        var _this = this;
        this.devicesService.getAllDevices()
            .then(function (data) {
            _this.devices = data;
        });
    };
    /**
     * Navigate to the detail page for this item.
     */
    SearchPage.prototype.openItem = function (item) {
        this.navCtrl.push(device_detail_1.DeviceDetailPage, {
            item: item
        });
    };
    return SearchPage;
}());
SearchPage = __decorate([
    core_1.Component({
        selector: 'page-search',
        templateUrl: 'devices-search.html',
    })
], SearchPage);
exports.SearchPage = SearchPage;
