"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var device_detail_1 = require("../devices-detail/device-detail");
var devices_search_1 = require("../devices-search/devices-search");
var moment = require("moment-timezone");
var ListMasterPage = (function () {
    function ListMasterPage(navCtrl, modalCtrl, loadingCtrl, devicesService, user, translateS) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.devicesService = devicesService;
        this.user = user;
        this.translateS = translateS;
        this.showLoading();
        this.loadDevices(null);
    }
    /**
     * get device info.
     */
    ListMasterPage.prototype.loadDevices = function (refresher) {
        var _this = this;
        this.devicesService.getAllDevices()
            .then(function (data) {
            _this.devices = data;
            _this.getDeviceInfos();
            if (_this.loading)
                _this.loading.dismissAll();
            if (refresher)
                refresher.complete();
        });
    };
    /**
     * get all devices.
     */
    ListMasterPage.prototype.loadAllDevices = function () {
        var _this = this;
        this.devicesService.getAllDevices()
            .then(function (data) {
            _this.devicedata = data;
        });
    };
    /**
     * Load device.
     */
    ListMasterPage.prototype.getDeviceInfos = function () {
        var _this = this;
        var _loop_1 = function (k) {
            this_1.devices[k]["status"] = false;
            if (this_1.devices[k]["reserved"]) {
                var reserved_1 = this_1.devices[k]["reserved"];
                var _loop_2 = function (i) {
                    if (!moment().tz("Europe/Berlin").isAfter(reserved_1[i]["to"])) {
                        this_1.devices[k]["status"] = true;
                    }
                    var reservationdata = {};
                    this_1.user.getuser(reserved_1[i]["userId"])
                        .then(function (data) {
                        reservationdata = data;
                        delete reservationdata["histories"];
                        delete reservationdata["devices"];
                        reservationdata["from"] = reserved_1[i]["from"];
                        reservationdata["to"] = reserved_1[i]["to"];
                        if (!_this.devices[k]["currentReservation"]) {
                            _this.devices[k]["currentReservation"] = [];
                        }
                        _this.devices[k].currentReservation.push(reservationdata);
                    });
                    /*          if (!moment().tz("Europe/Berlin").isAfter(reserved[i]["to"])) {
                                this.devices[k]["status"] = true;
                    
                                let reservationdata = {};
                                this.user.getuser(reserved[i]["userId"])
                                  .then(data => {
                                    reservationdata = data;
                                    delete reservationdata["histories"];
                                    delete reservationdata["devices"];
                                    reservationdata["from"] = reserved[i]["from"];
                                    reservationdata["to"] = reserved[i]["to"];
                                    if (!this.devices[k]["currentReservation"]) {
                                      this.devices[k]["currentReservation"] = [];
                                    }
                                    this.devices[k].currentReservation.push(reservationdata);
                                  });
                              }*/
                };
                for (var i in reserved_1) {
                    _loop_2(i);
                }
            }
        };
        var this_1 = this;
        for (var k in this.devices) {
            _loop_1(k);
        }
    };
    /**
     * The view loaded, let's query our items for the list
     */
    ListMasterPage.prototype.ionViewDidLoad = function () {
    };
    ListMasterPage.prototype.showLoading = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        setTimeout(function () {
            _this.loading.dismiss();
        }, 10000);
    };
    /**
     * Delete an item from the list of items.
     */
    ListMasterPage.prototype.deleteItem = function (item) {
        //this.items.delete(item);
    };
    /**
     * Navigate to the detail page for this item.
     */
    ListMasterPage.prototype.openItem = function (item) {
        this.navCtrl.push(device_detail_1.DeviceDetailPage, {
            item: item
        });
    };
    /**
     * Navigate to the search page.
     */
    ListMasterPage.prototype.opensearch = function () {
        this.navCtrl.push(devices_search_1.SearchPage);
    };
    return ListMasterPage;
}());
ListMasterPage = __decorate([
    core_1.Component({
        selector: 'page-list-master',
        templateUrl: 'devices-list.html',
    })
], ListMasterPage);
exports.ListMasterPage = ListMasterPage;
