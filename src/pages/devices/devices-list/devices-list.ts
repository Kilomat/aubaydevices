import {Component} from '@angular/core';
import {NavController, ModalController} from 'ionic-angular';
import {DeviceDetailPage} from '../devices-detail/device-detail';
import {SearchPage} from "../devices-search/devices-search";
import {TranslateService} from "@ngx-translate/core";
import * as moment from 'moment-timezone';
import {Devices} from "../../../providers/devices";
import {User} from "../../../providers/user";
import {Item} from "../../../models/item";
import {Display} from "../../../providers/display";


@Component({
  selector: 'page-list-master',
  templateUrl: 'devices-list.html',
})
export class ListMasterPage {
  public devices: any;
  public devicedata: any;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public devicesService: Devices,
              public user: User, public translateS: TranslateService, public displayService: Display) {

    this.displayService.showLoading();
    this.loadDevices(null);
  }

  /**
   * get device info.
   */
  loadDevices(refresher) {
    this.devicesService.getAllDevices()
      .then(data => {
        this.devices = data;
        this.getDeviceInfos();
        if (this.displayService.loading)
          this.displayService.loading.dismissAll();
        if (refresher)
          refresher.complete();
      });
  }

  /**
   * get all devices.
   */
  loadAllDevices() {
    this.devicesService.getAllDevices()
      .then(data => {
        this.devicedata = data;
      });
  }

  /**
   * Load device.
   */
  getDeviceInfos() {
    for (let k in this.devices) {
      this.devices[k]["status"] = false;
      if (this.devices[k]["reserved"]) {
        let reserved: any = this.devices[k]["reserved"];
        for (let i in reserved) {
          if (!moment().tz("Europe/Berlin").isAfter(reserved[i]["to"])) {
            this.devices[k]["status"] = true;
          }
          let reservationdata = {};
          this.user.getuser(reserved[i]["userId"])
            .then(data => {
              reservationdata = data;
              delete reservationdata["histories"];
              delete reservationdata["devices"];
              reservationdata["from"] = reserved[i]["from"];
              reservationdata["to"] = reserved[i]["to"];
              if (!this.devices[k]["currentReservation"]) {
                this.devices[k]["currentReservation"] = [];
              }
              this.devices[k].currentReservation.push(reservationdata);
            });
        }
      }
    }
  }


  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }



  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(DeviceDetailPage, {
      item: item
    });
  }

  /**
   * Navigate to the search page.
   */
  opensearch() {
    this.navCtrl.push(SearchPage);
  }

}
