"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var pages_1 = require("../../pages/pages");
var LoginPage = LoginPage_1 = (function () {
    function LoginPage(navCtrl, user, toastCtrl, translateService, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.user = user;
        this.toastCtrl = toastCtrl;
        this.translateService = translateService;
        this.loadingCtrl = loadingCtrl;
        // The account fields for the login form.
        this.account = {
            email: 'hbaha@aubay.com',
            password: 'Test1234'
        };
        this.translateService.get('LOGIN_ERROR').subscribe(function (value) {
            _this.loginErrorString = value;
        });
        this.translateService.get('LOGIN_ERROR_STATUS').subscribe(function (value) {
            _this.loginStatusErrorString = value;
        });
    }
    // Attempt to login in through our User service
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        this.showLoading();
        this.user.login(this.account).subscribe(function (resp) {
            _this.user.getuser(_this.user._user.idUser)
                .then(function (data) {
                if (data["status"]) {
                    _this.user._user.admin = data["admin"];
                    _this.navCtrl.push(pages_1.MainPage);
                }
                else {
                    _this.loading.dismissAll();
                    var toast = _this.toastCtrl.create({
                        message: _this.loginStatusErrorString,
                        duration: 3000,
                        position: 'top'
                    });
                    toast.present();
                }
            });
        }, function (err) {
            _this.navCtrl.push(LoginPage_1);
            // Unable to log in
            var toast = _this.toastCtrl.create({
                message: _this.loginErrorString,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
    };
    LoginPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    return LoginPage;
}());
LoginPage = LoginPage_1 = __decorate([
    core_1.Component({
        selector: 'page-my_login',
        templateUrl: 'login.html'
    })
], LoginPage);
exports.LoginPage = LoginPage;
var LoginPage_1;
