import {Component} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {MainPage} from '../../pages/pages';
import {User} from '../../providers/user';
import {NetworkService} from '../../providers/network';
import {TranslateService} from '@ngx-translate/core';
import {Display} from "../../providers/display";


@Component({
  selector: 'page-my_login',
  templateUrl: 'login.html'
})
export class LoginPage {

  Formvalid: boolean = false;
  // The account fields for the login form.
  account: { email: string, password: string } = { email: '',  password: '' };

  // Our translated text strings
  private loginErrorString: string;
  private loginStatusErrorString: string;

  constructor(public navCtrl: NavController, public user: User, public displayService: Display,
              public toastCtrl: ToastController, public translateService: TranslateService, public networkService : NetworkService) {

    this.networkService;

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
    this.translateService.get('LOGIN_ERROR_STATUS').subscribe((value) => {
      this.loginStatusErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doLogin() {
    this.displayService.showLoading();
    this.user.login(this.account).subscribe((resp) => {
      this.user.getuser(this.user._user.idUser)
        .then(data => {
          if(data["status"]){
            this.user._user.admin = data["admin"];
            this.navCtrl.push(MainPage);
          }
      else {
            this.displayService.loading.dismissAll();
            this.displayService.presentToast(this.loginStatusErrorString);
/*            let toast = this.toastCtrl.create({
              message: this.loginStatusErrorString,
              duration: 3000,
              position: 'top'
            });
            toast.present();*/
          }
        });
    }, (err) => {
      this.navCtrl.push(LoginPage);
      // Unable to log in
      this.displayService.presentToast(this.loginErrorString);
/*      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();*/
    });
  }

  formValid(){
    if (this.account.password && this.account.email){
      this.Formvalid = true;
    }
  }


/*  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }*/
}
