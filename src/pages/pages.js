"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var devices_list_1 = require("./devices/devices-list/devices-list");
var tabs_1 = require("./tabs/tabs");
var login_1 = require("./login/login");
var reservation_1 = require("./reservation/reservation");
var history_1 = require("./history/history");
var dashboard_1 = require("./dashboard/dashboard");
// The page the user lands on after opening the app and without a session
exports.FirstRunPage = login_1.LoginPage;
// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
exports.MainPage = tabs_1.TabsPage;
// The initial root pages for our tabs (remove if not using tabs)
exports.Tab1Root = devices_list_1.ListMasterPage;
exports.Tab2Root = reservation_1.ReservePage;
exports.Tab3Root = history_1.HistoryPage;
exports.Tab4Root = dashboard_1.DashboardPage;
