import {ListMasterPage} from './devices/devices-list/devices-list';
import {TabsPage} from './tabs/tabs';
import {LoginPage} from "./login/login";
import {ReservePage} from "./reservation/reservation";
import {HistoryPage} from "./history/history";
import {DashboardPage} from "./dashboard/dashboard";

// The page the user lands on after opening the app and without a session
//export const FirstRunPage = TabsPage;
export const FirstRunPage = LoginPage;

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = TabsPage;

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = ListMasterPage;
export const Tab2Root = ReservePage;
export const Tab3Root = HistoryPage;
export const Tab4Root = DashboardPage;
