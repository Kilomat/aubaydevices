/**
 * Created by hbaha on 29/05/2017.
 */

import {Component} from '@angular/core';
import {User} from "../../providers/user";
import {DashboardAdminPage} from "./dashboard-admin/dashboardAdmin";
import {DashboardUserPage} from "./dashboard-user/dashboardUser";
import {NavController} from "ionic-angular";


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  user: any;

  constructor(public navCtrl: NavController, userService: User) {
    this.user = userService._user.admin;
  }

  /**
   * The view loaded
   */
  ionViewDidEnter() {
    if (this.user)
      this.navCtrl.push(DashboardAdminPage);
    else
      this.navCtrl.push(DashboardUserPage);
  }
}

