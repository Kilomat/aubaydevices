/**
 * Created by hbaha on 29/05/2017.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var profile_1 = require("../profile/profile");
var users_create_1 = require("../../users/users-create/users-create");
var devices_create_1 = require("../../devices/devices-create/devices-create");
var list_users_1 = require("../../users/users-list/list-users");
var app_component_1 = require("../../../app/app.component");
var DashboardAdminPage = (function () {
    function DashboardAdminPage(alertCtrl, navCtrl, user, translateS) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.user = user;
        this.translateS = translateS;
    }
    DashboardAdminPage.prototype.logout = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('LOGOUT_MESSAGE')['value'],
            buttons: [
                {
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: this.translateS.get('LOGOUT_BUTTON')['value'],
                    handler: function () {
                        _this.user.logout();
                        _this.navCtrl.push(app_component_1.MyApp);
                        window.location.reload();
                    }
                }
            ]
        });
        alert.present();
    };
    DashboardAdminPage.prototype.openprofile = function () {
        this.navCtrl.push(profile_1.ProfilePage);
    };
    DashboardAdminPage.prototype.opennewuser = function () {
        this.navCtrl.push(users_create_1.UsersCreatePage);
    };
    DashboardAdminPage.prototype.opennewdevice = function () {
        this.navCtrl.push(devices_create_1.DevicesCreatePage);
    };
    DashboardAdminPage.prototype.openusers = function () {
        this.navCtrl.push(list_users_1.ListUsersPage);
    };
    return DashboardAdminPage;
}());
DashboardAdminPage = __decorate([
    core_1.Component({
        selector: 'page-dashboard-admin',
        templateUrl: 'dashboardAdmin.html'
    })
], DashboardAdminPage);
exports.DashboardAdminPage = DashboardAdminPage;
