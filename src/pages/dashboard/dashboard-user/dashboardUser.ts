/**
 * Created by hbaha on 29/05/2017.
 */

import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../../providers/user";
import {MyApp} from "../../../app/app.component";


@Component({
  selector: 'page-dashboard-user',
  templateUrl: 'dashboardUser.html'
})
export class DashboardUserPage {

  constructor(private alertCtrl: AlertController, public navCtrl: NavController, public user: User, public translateS: TranslateService) {
  }


  logout() {
    let alert = this.alertCtrl.create({
      message: this.translateS.get('LOGOUT_MESSAGE')['value'],
      buttons: [
        {
          text: this.translateS.get('CANCEL_BUTTON')['value'],
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: this.translateS.get('LOGOUT_BUTTON')['value'],
          handler: () => {
            this.user.logout();
            this.navCtrl.push(MyApp);
            window.location.reload();
          }
        }
      ]
    });
    alert.present();
  }

  openprofile(){
    this.navCtrl.push(ProfilePage);
  }
}
