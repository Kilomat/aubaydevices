"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var InputValidator_1 = require("../../users/users-create/InputValidator");
var ProfilePage = (function () {
    function ProfilePage(navCtrl, viewCtrl, formBuilder, user, translateS, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.user = user;
        this.translateS = translateS;
        this.alertCtrl = alertCtrl;
        this.users = {};
        this.loadUser();
        this.form = formBuilder.group({
            name: [''],
            email: [''],
            password: ['', [InputValidator_1.InputValidators.passwordValidate()]],
            phone: [''],
            _id: ['']
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (v) {
            _this.isReadyToSave = _this.form.valid;
        });
    }
    ProfilePage.prototype.loadUser = function () {
        this.users = this.user.dataUser;
    };
    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    ProfilePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    ProfilePage.prototype.done = function () {
        if (!this.form.valid) {
            return;
        }
        this.form.value['_id'] = this.user._user.idUser;
        this.user.changeUser(this.form.value);
        this.viewCtrl.dismiss(this.form.value);
    };
    ProfilePage.prototype.hasError = function (field, error) {
        var ctrl = this.form.get(field);
        return ctrl.dirty && ctrl.hasError(error);
    };
    ProfilePage.prototype.popupChangeProfile = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translateS.get('CHANGE_PROFILE_MESSAGE')['value'],
            buttons: [{
                    text: this.translateS.get('CANCEL_BUTTON')['value'],
                    role: 'cancel',
                    handler: function () {
                    }
                }, {
                    text: this.translateS.get('DONE_BUTTON')['value'],
                    handler: function () {
                        _this.done();
                    }
                }
            ]
        });
        alert.present();
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    core_1.Component({
        selector: 'page-profile',
        templateUrl: 'profile.html'
    })
], ProfilePage);
exports.ProfilePage = ProfilePage;
