import { Component } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AlertController,  NavController, ViewController} from 'ionic-angular';
import {User} from "../../../providers/user";
import {TranslateService} from "@ngx-translate/core";
import {InputValidators} from "../../users/users-create/InputValidator";

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  isReadyToSave: boolean;
  form: FormGroup;
  public users:any;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              formBuilder: FormBuilder,
              public user: User,
              public translateS:TranslateService,
              private alertCtrl: AlertController) {

    this.users = {};
    this.loadUser();
    this.form = formBuilder.group({
      name: [''],
      email: [''],
      password: ['', [InputValidators.passwordValidate()]],
      phone: [''],
      _id: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  loadUser(){
    this.users = this.user.dataUser;
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.form.value['_id'] = this.user._user.idUser;
    this.user.changeUser(this.form.value);
    this.viewCtrl.dismiss(this.form.value);
  }


  hasError(field: string, error: string) {
    const ctrl = this.form.get(field);
    return ctrl.dirty && ctrl.hasError(error);
  }

  popupChangeProfile() {

    let alert = this.alertCtrl.create({
      message: this.translateS.get('CHANGE_PROFILE_MESSAGE')['value'],
      buttons: [{
        text: this.translateS.get('CANCEL_BUTTON')['value'],
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: this.translateS.get('DONE_BUTTON')['value'],
        handler: () => {
          this.done();
        }
      }
      ]
    });
    alert.present();
  }

}
