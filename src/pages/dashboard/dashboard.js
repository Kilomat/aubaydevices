/**
 * Created by hbaha on 29/05/2017.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dashboardAdmin_1 = require("./dashboard-admin/dashboardAdmin");
var dashboardUser_1 = require("./dashboard-user/dashboardUser");
var DashboardPage = (function () {
    function DashboardPage(navCtrl, userService) {
        this.navCtrl = navCtrl;
        this.user = userService._user.admin;
    }
    /**
     * The view loaded
     */
    DashboardPage.prototype.ionViewDidEnter = function () {
        if (this.user)
            this.navCtrl.push(dashboardAdmin_1.DashboardAdminPage);
        else
            this.navCtrl.push(dashboardUser_1.DashboardUserPage);
    };
    return DashboardPage;
}());
DashboardPage = __decorate([
    core_1.Component({
        selector: 'page-dashboard',
        templateUrl: 'dashboard.html'
    })
], DashboardPage);
exports.DashboardPage = DashboardPage;
